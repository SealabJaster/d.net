﻿module System.ByteOrder;

/// Describes a certain byte order.
enum ByteOrder
{
	/// Big endian
	Big,

	/// Little endian
	Little,

	/// The system's native endianness
	Native = NativeByteOrder
}

private
{
	version(LittleEndian)
	{
		/// The system's native endianness
		const static ByteOrder NativeByteOrder = ByteOrder.Little;
	}
	else
	{
		/// The system's native endianness
		const static ByteOrder NativeByteORder = ByteOrder.Big;
	}
}