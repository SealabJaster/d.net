﻿module System.ICloneable;

/// Any inheriting class states that it supports cloning.
interface ICloneable
{
	Object Clone();
}