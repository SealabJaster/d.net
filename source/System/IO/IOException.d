﻿module System.IO.IOException;

public import System.SystemException;

/// The exception that is thrown when an I/O error occurs.
class IOException : SystemException
{
	@safe
	pure nothrow
	{
		/// Constructs an IOException with a given error message.
		/// 
		/// Parameters:
		/// 	message = Description of the error that occured.
		this(string message)
		{
			super(message);
		}
	}
}

