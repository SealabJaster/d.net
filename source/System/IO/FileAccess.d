﻿module System.IO.FileAccess;

/// Defines constatans for read, write, and read/write access to a file.
enum FileAccess : ubyte
{
	/// Allows read access to a file. Combine with $(B FileAccess.Write) for read/write access.
	Read = 1,

	/// Allows write access to a file. Combine with $(B FileAccess.Read) for read/write access.
	Write = 2,

	/// Allows read and write access to the file.
	ReadWrite = (Read | Write)
}