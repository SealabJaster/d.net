﻿module System.IO.FileStream;

private
{
	import std.file, std.stdio, std.string, std.exception;

	import System.IO.Path;
}

public
{
	import System.IO.Stream, System.IO.FileMode, System.IO.FileAccess,
		System.ArgumentNullException, System.IO.EndOfStreamException, System.IO.FileNotFoundException;
}

/// A stream class that provides an interface to read and write to files.
class FileStream : Stream
{
	private
	{
		File		_File;
		FileMode	_Mode;
		FileAccess	_Access;

		bool		_Disposed 	= false;
		bool		_IsOpen		= false;
	}

	public
	{
		/// Constructs a new FileStream with the specified path, creation mode, and read/write access.
		/// 
		/// Parameters:
		/// 	path = The path to the file.
		/// 	mode = How to open/create the file.
		/// 	access = Permission to write/read to the file.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B path) is null.
		/// Throws $(B System.NotSupportedException) if $(B mode) contains $(B FileMode.Append) and $(B access) grants $(B FileAccess.Read).
		/// Throws $(B System.IO.IOException) if a file at $(B path) already exists and $(B mode) contains $(B FileMode.CreateNew).
		/// Throws $(B System.IO.FileNotFoundException) if no tile exists at $(B path) and $(B mode) contains $(B FileMode.Open).
		/// May Throw $(B System.IO.IOException) if an unknown error occurs, please report the error message as this should be fixed to throw an exception from this library.
		this(string path, FileMode mode, FileAccess access)
		{
			enforce(path !is null,	new ArgumentNullException("path"));

			// These can change depending on what $(B mode) is.
			bool CanRead = cast(bool)(access & FileAccess.Read);
			bool CanWrite = cast(bool)(access & FileAccess.Write);

			// General bleh stuff.
			string OpenMode = ""; // "rb+", "wb", etc.
			this._Mode 		= mode;
			this._Access 	= access;
			this._IsOpen	= false;

			string FullPath = Path.GetFullPath!string(path);

			// If this is true by the end of the function then OpenMode start with a "w" instead of "r".
			bool Truncate = cast(bool)(mode & FileMode.Truncate);

			// This is used a lot, so may as well store it's value instead of re-calling exists.
			bool Exists = exists(FullPath);

			// Creates the file: This used to have different code in it, but I changed it, so it might seem odd.
			void Create()
			{
				Truncate = true;
			}

			// Giant if statement thing to do whatever we need to do before creating the File thing
			if(mode & FileMode.Append)
			{
				if(!Exists)
				{
					Create();
				}

				enforce(!CanRead,	new NotSupportedException("FileMode.Append can only be used with FileAccess.Write"));

				CanRead = false;
			}
			if(mode & FileMode.Create)
			{
				if(!Exists)
				{
					Create();
				}
				else
				{
					Truncate = true;
				}
			}
			if(mode & FileMode.CreateNew)
			{
				if(!Exists)
				{
					Create();
				}
				else
				{
					throw new IOException(format("File '%s' already exists. Please use FileMode.Create or make sure there is no file already existing.", FullPath));
				}
			}
			if(mode & FileMode.Open)
			{
				if(!Exists)
				{
					throw new FileNotFoundException(FullPath);
				}
			}
			if(mode & FileMode.OpenOrCreate)
			{
				if(!Exists)
				{
					Create();
				}
			}
			if(mode & FileMode.Truncate)
			{
				Truncate = true;
				CanRead = false;
			}

			// Setup what the stream can and can't do
			super(CanRead, true, false, CanWrite);

			// Make the file open mode string based on what we have
			if(Truncate)			OpenMode ~= "wb";
			else					OpenMode ~= "rb";
			if(CanRead && CanWrite) OpenMode ~= "+";

			// Debuguabgs
//			debug
//			{
//				import std.stdio;
//				writefln("Mode = %s | Access = %s | String = %s", mode, access, OpenMode);
//			}

			// Try to open the file, and if it fails then throw and IOException.
//			try
//			{
				this._File = File(FullPath, OpenMode);
				this._IsOpen = true;
//			}
//			catch(Exception ex)
//			{
//				throw new IOException(ex.msg);
//			}
		}

		/// Write out data from the given buffer into the file.
		/// 
		/// Parameters:
		/// 	buffer = The data to write.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		void Write(T)(T[] buffer)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();

			enforce(buffer !is null, new ArgumentNullException("buffer"));

			this._File.rawWrite(buffer);
		}
		
		/// Read data from the file into the given buffer. Returning how many of $(B T) were actually read.
		/// 
		/// Parameters:
		/// 	buffer = The buffer to read into.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	How many $(B T) were read into $(B buffer).
		long Read(T)(T[] buffer)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();

			enforce(buffer !is null, new ArgumentNullException("buffer"));

			return this._File.rawRead(buffer).length;
		}

		/// Copy the stream's data to the given stream.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream)
		{
			this.CopyTo(stream, this.Length);
		}
		
		/// Copy the stream's data to the given stream by using a buffer with a given size as the transfer medium.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 	bufferSize = How many bytes can be copied at a time.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanRead) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream, size_t bufferSize)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();
			
			enforce(stream !is null,	new ArgumentNullException("stream"));
			enforce(stream.CanRead,		new InvalidOperationException("The given stream does not support reading."));
			enforce(!stream.IsDisposed,	new ObjectDisposedException("The given stream has been disposed."));
			
			auto OldPosition = this.Position;
			this.Position = 0;
			
			ubyte[] Buffer;
			Buffer.length = bufferSize;
			
			while(true)
			{
				auto Read = this.Read!ubyte(Buffer);
				stream.Write!ubyte(Buffer[0..cast(size_t)Read]);
				
				if(Read < bufferSize)
				{
					break;
				}
			}
			
			this.Position = OldPosition;
		}

		/// Get the read/write permission that the FileStream has.
		@property
		FileAccess Access()
		{
			return this._Access;
		}

		/// Get the method the FileStream's file was opened/created in.
		@property
		FileMode Mode()
		{
			return this._Mode;
		}

		/// Get whether the stream still has it's file open.
		@property
		bool IsOpen()
		{
			return this._IsOpen;
		}
	}

	public override
	{		
		/// Closes the file and marks the FileStream as disposed.
		void Dispose()
		{
			if(!this.IsDisposed)
			{
				if(this._IsOpen)
				{
					this._IsOpen = false;
					this._File.close();
				}

				this._Disposed = true;
			}
		}
		
		/// Flush the file buffer.
		void Flush()
		{
			this._File.flush();
		}
		
		/// Read a single byte from the file
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the file.
		ubyte ReadByte()
		{
			super.ThrowIfCantRead();
			super.ThrowIfDisposed();

			ubyte[1] Buffer;
			this.Read!ubyte(Buffer);

			return Buffer[0];
		}
		
		/// Reads a single from the stream without advancing it's position.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the stream.
		ubyte Peek()
		{
			super.ThrowIfDisposed();
			super.ThrowIfCantRead();

			auto ToReturn = this.ReadByte();
			this.Position = this.Position - 1;

			return ToReturn;
		}
		
		/// Seek using the given offset and origin to a position in the stream's buffer.
		/// 
		/// Parameters:
		/// 	offset = The offset to move from $(B origin).
		/// 	origin = Where the seek from in the stream.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentException) if the stream's position after seeking it a negative value.
		/// Throws $(B System.NotSupportedException) if $(B this.CanSeek) is false.
		/// 
		/// Returns:
		/// 	The Stream's new position.
		long Seek(long offset, SeekOrigin origin)
		{
			this._File.seek(offset, origin);
			return this.Position;
		}
		
		/// Set the size of the file.
		/// 
		/// Parameters:
		/// 	length = The size to set thefile.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if the stream doesn't support this function.
		void SetLength(size_t length)
		{
			super.ThrowIfDisposed();

			// TODO: Complete the method
			throw new NotSupportedException("Unimplemented");
		}
		
		/// Write a single byte into the stream.
		/// 
		/// Parameters:
		/// 	data = The byte to write.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		void WriteByte(ubyte data)
		{
			super.ThrowIfDisposed();
			super.ThrowIfCantWrite();

			this.Write!ubyte([data]);
		}
		
		@property
		{
			size_t 	Length()		
			{
				auto Pos = this.Position;
				this.Seek(0, SeekOrigin.End);

				auto Size = this.Position;
				this.Position = Pos;

				return Size;
			}
			size_t 	Position()		{return cast(size_t)this._File.tell;}
			size_t 	ReadTimeout()	{return 0;}
			size_t 	WriteTimeout()	{return 0;}
			
			void 	Length(size_t length)		{this.SetLength(length);}
			void 	Position(size_t position)	{this.Seek(cast(size_t)position, SeekOrigin.Begin);}
			void 	ReadTimeout(size_t timeout)	{}
			void 	WriteTimeout(size_t timeout){}
			
			/// Returns whether the stream has been disposed.
			bool	IsDisposed() {return this._Disposed;} 
			
			/// Returns whether the stream is at the end of it's buffer.
			bool	EoF() {return (this.Position <= this.Length);}
		}
	}
}
unittest
{
	import std.stdio, System.IO.MemoryStream;

	FileStream Stream;
	ulong Test = 0x0123456789ABCDEF;
	string PathT = "unittests/FileTest.bin";

	Stream = new FileStream(PathT, FileMode.Create, FileAccess.ReadWrite);
	Stream.Write([Test]);
	Stream.Flush();
//
//	writefln("Length = %d", Stream.Length);

	assert(Stream.Length == ulong.sizeof);
	assert(Stream.Position == ulong.sizeof);
	assert(Stream.EoF);
	
	ulong[1] Buffer;
	Stream.Position = 0;

//	writefln("Position = %d", Stream.Position);

	auto Temp = Stream.Read!ulong(Buffer);
	assert(Temp == Buffer.length, format("Read in = %d", Temp));
	assert(Buffer[0] == Test);

	uint Count = 0; // Used to make sure the try catch stuff works
	assert(Stream.ReadTimeout == Stream.WriteTimeout);
	try
	{
		Stream.Length = 0;
	}
	catch(NotSupportedException)
	{
		Count++;
	}

	auto Thing = new MemoryStream(0, true);
	Stream.Flush();
	Stream.CopyTo(Thing);

	Thing.Position = 0;
	Thing.Read!ulong(Buffer);
	assert(Buffer[0] == Test);

	/************The quest for 100% coverage************/
	auto Pos = Stream.Position;
	Stream.WriteByte(0xFF);
	Stream.Position = Pos;

	assert(Stream.Peek() == 0xFF);
	assert(Stream.ReadByte() == 0xFF);
	assert(Stream.IsOpen);

	void PleaseTheLordOfCoverage(string S)()
	{
		mixin(format("auto X = Stream.%s;", S));
	}
	PleaseTheLordOfCoverage!"Access"();
	PleaseTheLordOfCoverage!"Mode"();

	Stream.Dispose();

	void PleaseTheLordOfCoverage2(FileMode mode, bool Del)
	{
		if(Del && exists(PathT)) remove(PathT.toStringz());
		Stream = new FileStream(PathT, mode, mode == FileMode.Append ? FileAccess.Write : FileAccess.ReadWrite);
		Stream.Dispose();
	}
	PleaseTheLordOfCoverage2(FileMode.Create, false);
	PleaseTheLordOfCoverage2(FileMode.Create, true);
	PleaseTheLordOfCoverage2(FileMode.CreateNew, true);
	PleaseTheLordOfCoverage2(FileMode.Append, true);
	PleaseTheLordOfCoverage2(FileMode.Append, false);

	try
	{
		PleaseTheLordOfCoverage2(FileMode.CreateNew, false);
	}
	catch(IOException)
	{
		Count++;

		try
		{
			PleaseTheLordOfCoverage2(FileMode.Open, true);
		}
		catch(FileNotFoundException)
		{
			Count++;
		}
	}

	assert(Count == 3);
	PleaseTheLordOfCoverage2(cast(FileMode)(FileMode.OpenOrCreate | FileMode.Truncate), true);
	/*********************************/
}