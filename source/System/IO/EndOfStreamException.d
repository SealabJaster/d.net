﻿module System.IO.EndOfStreamException;

public
{
	import System.IO.IOException;
}

/// The exception that is thrown when attempting an operation past the end of a $(B Stream).
class EndOfStreamException : Exception
{
	@safe
	pure nothrow
	{
		/// Constructs an EndOfStreamException with a given description of the attempted operation.
		/// 
		/// Parameters:
		/// 	operation = The operation (Such as "read", "seek", etc.) that caused the exception.
		this(string operation)
		{
			super("Attempted to " ~ operation ~ " past the end of the stream.");
			//super(format("Attempted to %s past the end of the stream.", operation));
		}
	}
}