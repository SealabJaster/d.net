﻿module System.IO.FileNotFoundException;

public
{
	import System.IO.IOException;
}
// The exception that is thrown when a requested file does not exist.
class FileNotFoundException : IOException
{
	@safe
	pure nothrow
	{
		/// Construct a new FileNotFoundException that displays a message using the given path string.
		/// 
		/// Parameters:
		/// 	file = The path string to the file that caused the exception.
		this(string file)
		{
			super("Unable to find file '" ~ file ~ "'.");
			//super(format("Unable to find file '%s'", file));
		}
	}
}