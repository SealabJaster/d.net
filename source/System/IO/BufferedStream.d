﻿module System.IO.BufferedStream;

private
{
	import std.exception;

	import System.BitConverter;
}

public
{
	import System.IO.Stream;
}

// I have no idea how reading things in with a buffer would work, and I don't think I quite understand the use of Buffered stuff anyway. So this class may be really strange to use.
/// A utility stream that provides the ability to write to a buffer before writing to a base stream
class BufferedStream(T : Stream) : Stream
{
	private
	{
		ubyte[]	_Buffer;
		size_t	_Index;

		T		_Base;
		bool	_Disposed = false;
	}

	public
	{
		/// Constructs a new BufferedStream that writes to the given stream when it's buffer(With a specified size) cannot store anymore data.
		/// 
		/// Parameters:
		/// 	baseStream = The stream to write to.
		/// 	bufferSize = The size of the BufferedStream's buffer.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B baseStream) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B baseStream) has been disposed.
		/// Throws $(B System.NotSupportedException) if $(B baseStream) does not support writing.
		this(T baseStream, size_t bufferSize = 4096)
		{
			enforce(baseStream !is null, 	new ArgumentNullException("baseStream"));
			enforce(!baseStream.IsDisposed, new ObjectDisposedException("The given stream has been disposed."));
			enforce(baseStream.CanWrite,	new NotSupportedException("The given stream does not support writing."));

			this._Buffer.length = bufferSize;
			this._Base = baseStream;

			super(false, baseStream.CanSeek, baseStream.CanTimeout, true);
		}

		/// Get the BufferedStream's base stream.
		@property
		T BaseStream()
		{
			return this._Base;
		}
	}

	public override
	{		
		/// Flushes the BufferedStream and then disposes it's buffer.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		void Dispose()
		{
			this.ThrowIfDisposed();

			this.Flush();
			this._Buffer = null;
			this._Disposed = true;
		}
		
		/// Flush the stream's buffer if needed. Otherwise, do nothing.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		void Flush()
		{
			this.ThrowIfDisposed();

			if(this._Index > 0)
			{
				this._Base.Write(this._Buffer[0..this._Index]);
				this._Buffer[] = 0;
				this._Index = 0;
			}
		}
		
		/// (Not sure how reading would work with a BufferedStream that can be read and written from, so this just crashes if used)
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the stream.
		ubyte ReadByte()
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();
			assert(false);
		}
		
		/// (Not sure how reading would work with a BufferedStream that can be read and written from, so this just crashes if used)
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the stream.
		ubyte Peek()
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();
			assert(false);
		}
		
		/// Seek using the given offset and origin to a position in the stream's buffer(Calls the function for the base stream of the BufferedStream).
		/// 
		/// Parameters:
		/// 	offset = The offset to move from $(B origin).
		/// 	origin = Where the seek from in the stream.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentException) if the stream's position after seeking it a negative value.
		/// Throws $(B System.NotSupportedException) if $(B this.CanSeek) is false.
		/// 
		/// Returns:
		/// 	The Stream's new position.
		long Seek(long offset, SeekOrigin origin)
		{
			this.ThrowIfCantSeek();
			return this._Base.Seek(offset, origin);
		}
		
		/// Set the size of the stream's buffer(Calls the function for the base stream of the BufferedStream).
		/// 
		/// Parameters:
		/// 	length = The size to set the stream's buffer.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if the stream doesn't support this function.
		void SetLength(size_t length)
		{
			this._Base.SetLength(length);
		}
		
		/// Write a single byte into the stream.
		/// 
		/// Parameters:
		/// 	data = The byte to write.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		void WriteByte(ubyte data)
		{
			this.Write([data]);
		}
		
		@property
		{
			/// Calls the BufferedStream's BaseStream's function.
			size_t 	Length()		{return this._Base.Length;}
			/// Ditto
			size_t 	Position()		{return this._Base.Position;}
			/// Ditto
			size_t 	ReadTimeout()	{return this._Base.ReadTimeout;}
			/// Ditto
			size_t 	WriteTimeout()	{return this._Base.WriteTimeout;}

			/// Ditto
			void 	Length(size_t length)		{this._Base.Length = length;}
			/// Ditto
			void 	Position(size_t position)	{this.Seek(position, SeekOrigin.Begin);}
			/// Ditto
			void 	ReadTimeout(size_t timeout)	{this._Base.ReadTimeout = timeout;}
			/// Ditto
			void 	WriteTimeout(size_t timeout){this._Base.WriteTimeout = timeout;}
			
			/// Returns whether the BufferedStream has been disposed.
			bool	IsDisposed() {return this._Disposed;}
			
			/// Calls the BufferedStream's BaseStream's function.
			bool	EoF() {return this._Base.EoF;}
		}
	}
	
	public
	{
		/// Write out data from the given buffer into the BufferedStream's buffer, and if the buffer is too full to contain the data, then the buffer and the given data is all written to the stream.
		/// 
		/// Parameters:
		/// 	buffer = The data to write.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		void Write(T)(T[] buffer)
		{
			this.ThrowIfDisposed();
			enforce(buffer !is null, new ArgumentNullException("buffer"));

			size_t ByteSize = (buffer.length * T.sizeof);

			// The buffer won't hold the data, so write out the buffer and the given data
			if((this._Index + ByteSize) > this._Buffer.length)
			{
				this.Flush();
				this._Base.Write(buffer);
			}
			else
			{
				this._Index += ByteSize;
				this._Buffer[this._Index - ByteSize..this._Index] = BitConverter.Get!ubyte(buffer);
			}
		}

		/// Read data from the stream into the given buffer. Returning how many of $(B T) were actually read.
		/// 
		/// Parameters:
		/// 	buffer = The buffer to read into.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	How many $(B T) were read into $(B buffer).
		long Read(T)(T[] buffer)
		{
			throw new NotSupportedException("BufferedStream does not support reading. Use it's BaseStream property if needed.");
		}
		
		/// Copy the stream's data to the given stream(Note: this calls the BufferedStream's base stream's function).
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream)
		{
			this._Base.CopyTo(stream);
		}
		
		/// Copy the stream's data to the given stream by using a buffer with a given size as the transfer medium(Note: this calls the BufferedStream's base stream's function).
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 	bufferSize = How many bytes can be copied at a time.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream, size_t bufferSize)
		{
			this._Base.CopyTo(stream, bufferSize);
		}
	}
}
unittest
{
	import System.IO;

	// Generic unittest stuff(I should probably just make a version(unittest) function in Stream.d for this...)
	ubyte[] Test2 = [0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF];
	ulong Test = 0x0123456789ABCDEF;
	
	auto Stream = new BufferedStream!MemoryStream(new MemoryStream(0, true), 8);
	Stream.Write([Test]);

	assert(Stream.Length == 0);
	Stream.Flush();
	
	assert(Stream.Length == ulong.sizeof);
	assert(Stream.Position == ulong.sizeof);
	assert(Stream.EoF);
	
	ulong[1] Buffer;
	Stream.Position = 0;
	assert(Stream.BaseStream.Read!ulong(Buffer) == Buffer.length);
	assert(Buffer[0] == Test);
	
	Stream.BaseStream.Buffer = Test2;
	assert(Stream.BaseStream.Read!ulong(Buffer) == Buffer.length);
	
	version(BigEndian)
	{
		assert(Buffer[0] == Test);
	}
	else
	{
		assert(Buffer[0] == 0xEFCDAB8967452301);
	}
	
	Stream.Position = 0;

	auto Stream2 = new MemoryStream(Test2.length, false);
	Stream.CopyTo(Stream2);
	Stream2.Position = 0;
	
	ubyte[] Temp;
	Temp.length = Test2.length;
	assert(Stream2.Read!ubyte(Temp) == Temp.length);
	assert(Temp == Test2);

	// And now to test that the buffer actually works.
	Stream.SetLength(0);

	Stream.Write(Test2); // It should be stored in the buffer.
	assert(Stream.BaseStream.Length == 0);

	Stream.WriteByte(0x00); // Now it should all be sent out
	assert(Stream.BaseStream.Buffer == (Test2 ~ 0x00));

	// And finally, the quest for 100% code completeion
	try{Stream.Read(Buffer); assert(false);}catch(NotSupportedException ex){}
	try{Stream.Peek(); assert(false);}catch(NotSupportedException ex){}

	Stream.ReadTimeout = 0;  assert(Stream.ReadTimeout == 0);
	Stream.WriteTimeout = 0; assert(Stream.WriteTimeout == 0);
	Stream.Length = 0;		 assert(Stream.Length == 0);

	Stream.Dispose();
}