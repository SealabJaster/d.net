﻿module System.IO.Stream;

private
{
}

public
{
	import System.IDisposable, System.IO.SeekOrigin, System.ArgumentNullException,
		System.NotSupportedException, System.ObjectDisposedException;
}

abstract class Stream : IDisposable
{
	private
	{
		bool	_CanRead;
		bool	_CanSeek;
		bool	_CanTimeout;
		bool	_CanWrite;
	}

	public abstract
	{
		/// Free the stream's resoruces, intended to be used once the stream no longer needs to be used.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		void 	Dispose();

		/// Flush the stream's buffer if needed. Otherwise, do nothing.
		void 	Flush();

		/// Read a single byte from the stream
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the stream.
		ubyte	ReadByte();

		/// Reads a single from the stream without advancing it's position.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	The next byte in the stream.
		ubyte	Peek();

		/// Seek using the given offset and origin to a position in the stream's buffer.
		/// 
		/// Parameters:
		/// 	offset = The offset to move from $(B origin).
		/// 	origin = Where the seek from in the stream.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentException) if the stream's position after seeking it a negative value.
		/// Throws $(B System.NotSupportedException) if $(B this.CanSeek) is false.
		/// 
		/// Returns:
		/// 	The Stream's new position.
		long	Seek(long offset, SeekOrigin origin);

		/// Set the size of the stream's buffer.
		/// 
		/// Parameters:
		/// 	length = The size to set the stream's buffer.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if the stream doesn't support this function.
		void	SetLength(size_t length);

		/// Write a single byte into the stream.
		/// 
		/// Parameters:
		/// 	data = The byte to write.
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		void	WriteByte(ubyte data);

		@property
		{
			size_t 	Length();
			size_t 	Position();
			size_t 	ReadTimeout();
			size_t 	WriteTimeout();

			void 	Length(size_t length);
			void 	Position(size_t position);
			void 	ReadTimeout(size_t timeout);
			void 	WriteTimeout(size_t timeout);

			/// Returns whether the stream has been disposed.
			bool	IsDisposed();

			/// Returns whether the stream is at the end of it's buffer.
			bool	EoF();
		}
	}

	public
	{
		/// Write out data from the given buffer into the stream.
		/// 
		/// Parameters:
		/// 	buffer = The data to write.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		void	Write(T)(T[] buffer){throw new InvalidOperationException("Please implement this function.");}

		/// Read data from the stream into the given buffer. Returning how many of $(B T) were actually read.
		/// 
		/// Parameters:
		/// 	buffer = The buffer to read into.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// 
		/// Returns:
		/// 	How many $(B T) were read into $(B buffer).
		long	Read(T)(T[] buffer){throw new InvalidOperationException("Please implement this function.");}

		/// Copy the stream's data to the given stream.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream){throw new InvalidOperationException("Please implement this function.");}
		
		/// Copy the stream's data to the given stream by using a buffer with a given size as the transfer medium.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 	bufferSize = How many bytes can be copied at a time.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream, size_t bufferSize){throw new InvalidOperationException("Please implement this function.");}
	}

	@property
	public
	{
		/// Return whether the stream can read data.
		bool CanRead() 		{return this._CanRead;}

		/// Return whether the stream can write data
		bool CanWrite() 	{return this._CanWrite;}

		/// Return whether the stream can timeout.
		bool CanTimeout()	{return this._CanTimeout;}

		/// Return whether the stream can seek through it's data.
		bool CanSeek()		{return this._CanSeek;}
	}

	protected
	{
		this(bool canRead, bool canSeek, bool canTimeout, bool canWrite)
		{
			this._CanRead 		= canRead;
			this._CanSeek 		= canSeek;
			this._CanTimeout 	= canTimeout;
			this._CanWrite		= canWrite;
		}

		void ThrowIfDisposed()
		{
			if(this.IsDisposed)
			{
				throw new ObjectDisposedException("The stream has been dipsosed.");
			}
		}

		void ThrowIfCantRead()
		{
			if(!this.CanRead)
			{
				throw new NotSupportedException("This stream does not support reading.");
			}
		}

		void ThrowIfCantWrite()
		{
			if(!this.CanWrite)
			{
				throw new NotSupportedException("This stream does not support writing.");
			}
		}

		void ThrowIfCantSeek()
		{
			if(!this.CanSeek)
			{
				throw new NotSupportedException("This stream does not support seeking.");
			}
		}
	}
}

