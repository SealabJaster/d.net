﻿module System.IO.MemoryStream;

private
{
	import std.exception;
}

public
{
	import System.IO.Stream, System.IO.EndOfStreamException;
}

/// A stream class that allows you to write and read data in memory.
class MemoryStream : Stream
{
	private
	{
		ubyte[]	_Buffer;
		bool	_CanResize;

		size_t	_Position;
		bool	_Disposed = false;
	}

	public
	{
		/// Constructs a $(B MemoryStream) that peforms it's actions on a given buffer, that may or may not resize the buffer if needed during writing operations.
		/// 
		/// Parameters:
		/// 	buffer = The buffer that the MemoryStream should use.
		/// 	canResize = True if the MemoryStream is allowed to resize $(B buffer) during writing operations, as well as for $(B Stream.SetLength) calls. False to disallow this behaviour.
		this(ubyte[] buffer, bool canResize)
		{
			super(true, true, false, true);

			this._Buffer 	= buffer;
			this._CanResize = canResize;
		}

		/// Constructs a $(B MemoryStream) that peforms it's actions on a buffer with a given size, that may or may not resize the buffer if needed during writing operations.
		/// 
		/// Parameters:
		/// 	bufferSize = The initial size of the MemoryStream's buffer.
		/// 	canResize = True if the MemoryStream is allowed to resize $(B buffer) during writing operations, as well as for $(B Stream.SetLength) calls. False to disallow this behaviour.
		this(size_t bufferSize, bool canResize)
		{
			ubyte[] Temp; Temp.length = bufferSize;
			this(Temp, canResize);
		}

		/// Empties the Stream's buffer.
		/// If the MemoryStream can resize it's buffer, then it's buffer's length and the stream's position is set to 0.
		/// If the MemoryStream can't resize it's buffer, then it will zero-out it's buffer.
		void Clear()
		{
			if(this._CanResize)
			{
				this._Buffer.length = 0;
			}
			else
			{
				this._Buffer[] = 0;
			}
			
			this._Position = 0;
		}

		override
		{
			/// Empties the Stream's buffer.
			void Dispose()
			{
				// Maybe I should tell the GC to free the buffer?
				this.Clear();
				this._Disposed = true;
			}
			
			/// Does nothing for $(B MemoryStream)s
			void 	Flush(){}
			
			/// Read a single byte from the stream
			/// 
			/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
			/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
			/// Throws $(B System.IO.EndOfStreamException) if $(B this.EoF) is true.
			/// 
			/// Returns:
			/// 	The next byte in the stream.
			ubyte ReadByte()
			{
				super.ThrowIfDisposed();
				super.ThrowIfCantRead();

				enforce(!this.EoF, new EndOfStreamException("read"));

				return this._Buffer[this._Position++];
			}
			
			/// Reads a single from the stream without advancing it's position.
			/// 
			/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
			/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
			/// Throws $(B System.EndOfStream) if $(B this.EoF) is true.
			/// 
			/// Returns:
			/// 	The next byte in the stream.
			ubyte Peek()
			{
				super.ThrowIfDisposed();
				super.ThrowIfCantRead();
				
				enforce(!this.EoF, new EndOfStreamException("read"));

				return this._Buffer[this._Position];
			}
			
			/// Seek using the given offset and origin to a position in the stream's buffer.
			/// 
			/// Parameters:
			/// 	offset = The offset to move from $(B origin).
			/// 	origin = Where the seek from in the stream.
			/// 
			/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
			/// Throws $(B System.ArgumentException) if the stream's position after seeking it a negative value.
			/// Throws $(B System.NotSupportedException) if $(B this.CanSeek) is false.
			/// 
			/// Returns:
			/// 	The Stream's new position.
			long Seek(long offset, SeekOrigin origin)
			{
				super.ThrowIfDisposed();
				super.ThrowIfCantSeek();

				switch(origin)
				{
					case SeekOrigin.Begin:
						this._Position = cast(size_t)offset;
						break;

					case SeekOrigin.Current:
						this._Position += offset;
						break;

					case SeekOrigin.End:
						this._Position = cast(size_t)(this._Buffer.length - (offset + 1));
						break;

					default: assert(false);
				}

				enforce(this._Position >= 0, new ArgumentException("The offset has set the stream's position to a negative value", "offset"));

				return this._Position;
			}
			
			/// Set the size of the stream's buffer.
			/// The stream's position isn't reset, so it may be in an invalid position afterwards.
			/// 
			/// Parameters:
			/// 	length = The size to set the stream's buffer.
			/// 
			/// 
			/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
			/// Throws $(B System.NotSupportedException) if $(B this.CanResize) is false.
			void SetLength(size_t length)
			{
				super.ThrowIfDisposed();

				enforce(this.CanResize, new NotSupportedException("This current instance of MemoryStream has been disallowed from resizing it's buffer."));

				this._Buffer.length = length;
			}
			
			/// Write a single byte into the stream.
			/// 
			/// Parameters:
			/// 	data = The byte to write.
			/// 
			/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
			/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
			void WriteByte(ubyte data)
			{
				this.Write!ubyte([data]);
			}

			@property
			{
				/// Get the size of the MemoryStream's buffer
				size_t 	Length() 		{return this._Buffer.length;}

				/// Get the current position of where the MemoryStream is in it's buffer.
				size_t 	Position() 		{return this._Position;}

				size_t 	ReadTimeout()	{return 0;}
				size_t 	WriteTimeout()	{return 0;}

				/// Set the length of the MemoryStream's buffer(The position isn't moved, please note this)
				void 	Length(size_t length)		{this._Buffer.length = length;}

				/// Set the position of where the MemoryStream is in it's buffer.
				void 	Position(size_t position)	{this.Seek(position, SeekOrigin.Begin);}

				void 	ReadTimeout(size_t timeout)	{}
				void 	WriteTimeout(size_t timeout){}
				
				/// Returns whether the stream has been disposed.
				bool	IsDisposed() {return this._Disposed;}

				/// Returns whether the stream is at the end of it's buffer.
				bool	EoF() {return (this._Position >= this._Buffer.length);}
			}
		}

		/// Copy the stream's data to the given stream.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream)
		{
			this.CopyTo(stream, this._Buffer.length);
		}
		
		/// Copy the stream's data to the given stream by using a buffer with a given size as the transfer medium.
		/// 
		/// Parameters:
		/// 	stream = The stream to copy to.
		/// 	bufferSize = How many bytes can be copied at a time.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.NotSupportedException) if $(B stream.CanRead) is false, or if $(B this.CanWrite) is false.
		/// Throws $(B System.ObjectDisposedException) if $(B stream.IsDisposed) is true, or if $(B this.IsDisposed) is true.
		void CopyTo(T : Stream)(T stream, size_t bufferSize)
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();
			
			enforce(stream !is null,	new ArgumentNullException("stream"));
			enforce(stream.CanRead,		new InvalidOperationException("The given stream does not support reading."));
			enforce(!stream.IsDisposed,	new ObjectDisposedException("The given stream has been disposed."));
			
			// Slightly memory efficient?
			if(bufferSize == this._Buffer.length)
			{
				stream.Write!ubyte(this._Buffer);
			}
			else
			{
				size_t Count = 0; // This is how the universe was born
				
				while(Count < this._Buffer.length)
				{
					// If we're writing the last piece of data, then make sure we don't cause an out of range evil thing of crashiness
					if((Count + bufferSize) >= this._Buffer.length)
					{
						stream.Write!ubyte(this._Buffer[Count..$]);
					}
					else
					{
						stream.Write!ubyte(this._Buffer[Count..(Count + bufferSize)]);
					}
					
					Count += bufferSize;
				}
			}
		}

		/// Read data from the stream into the given buffer. Returning how many of $(B T) were actually read.
		/// 
		/// Parameters:
		/// 	buffer = The buffer to read into.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanRead) is false.
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true and there isn't enough bytes to read to create a single $(B T)
		/// 
		/// Returns:
		/// 	How many $(B T) were read into $(B buffer).
		long Read(T)(T[] buffer)
		if(!is(T == class))
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantRead();

			enforce(buffer !is null, 	new ArgumentNullException("buffer"));
			enforce(!this.EoF, 		 	new InvalidOperationException("Attempted to start a read operation when the stream was at the end of it's buffer."));
			
			// Convinienaceneece
			size_t TypeSize 	= T.sizeof;
			size_t Total		= (TypeSize * buffer.length);
			size_t FuturePos 	= (this._Position + Total);

			// If we're gonna go out of bounds, then read as much as we can
			if(FuturePos > this._Buffer.length)
			{
				// Too lazy right now to make a smart way to calculate this
				// Figure out the safe amount of bytes we're allowed to use
//				while(Total > this._Buffer.length)
//				{
//					Total -= TypeSize;
//				}

				// In my head the numbers in this are correct... Let's hope it doesn't cause issues.
				// Work out the difference between where our position would be in the stream, and the length of our buffer.
				// Then multiply TypeSize by the difference and we should be in range again.
				Total -= ((FuturePos - this.Length) * TypeSize);
			}

//			debug
//			{
//				import std.stdio;
//				writefln("Pos: %d | Total = %d | ReturnValue = %d | TypeSize = %d | Length = %d", this._Position, Total, Total / TypeSize, TypeSize, this.Length);
//			}
			
			buffer[0..(Total / TypeSize)] = cast(T[])(cast(void[])this._Buffer[this._Position..(this._Position + Total)]);
			this._Position += Total;

			return (Total / TypeSize);
		}

		/// Write out data from the given buffer into the stream.
		/// If data is to be written to the buffer, but the data would need a resize and the stream isn't allowed to resize then only a partial amount of the given buffer's data is written.
		/// 
		/// Parameters:
		/// 	buffer = The data to write.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.NotSupportedException) if $(B this.CanWrite) is false.
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		void Write(T)(T[] buffer)
		if(!is(T == class))
		{
			this.ThrowIfDisposed();
			this.ThrowIfCantWrite();			
			enforce(buffer !is null, 	new ArgumentNullException("buffer"));
			
			size_t	TypeSize 	= T.sizeof;
			size_t 	Total 		= (TypeSize * buffer.length);

			this._Position += Total;

			// Used further down, I do the check here, because of reasons.
			bool 	Flag 		= (this._Position > this._Buffer.length);
			auto	OldPosition = (this._Position - Total);

//			debug
//			{
//				import std.stdio;
//				writefln("TypeSize = %d | Total = %d | Pos = %d | OldPos = %d | Flag = %s | Length = %d", TypeSize, Total, this._Position, OldPosition, Flag, this.Length);
//			}

			// Resize if we must/can
			if(Flag)
			{
				if(this.CanResize)
				{
					this._Buffer.length = this._Position;
					this._Buffer[(this._Position - Total)..this._Position] = cast(ubyte[])(cast(void[])buffer);
				}
				else
				{
					Total = (this.Length - OldPosition);
					this._Buffer[OldPosition..$] = (cast(ubyte[])(cast(void[])buffer))[0..Total];
				}
			}
			else
			{
				this._Buffer[(this._Position - Total)..this._Position] = cast(ubyte[])(cast(void[])buffer);
			}
		}

		/// Returns whether the MemoryStream is allowed to resize it's buffer.
		@property
		bool CanResize()
		{
			return this._CanResize;
		}

		/// Gets the MemoryStream's buffer.
		@property
		const(ubyte[]) Buffer()
		{
			return this._Buffer;
		}

		/// Sets the MemoryStream's buffer. Note that this resets the MemoryStream's position.
		@property
		void Buffer(ubyte[] buffer)
		{
			this._Buffer = buffer;
			this.Position = 0;
		}
	}
}
unittest
{
	import std.stdio;

	ubyte[] Test2 = [0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF];
	ulong Test = 0x0123456789ABCDEF;

	auto Stream = new MemoryStream(0, true);
	Stream.Write([Test]);

	assert(Stream.Length == ulong.sizeof);
	assert(Stream.Position == ulong.sizeof);
	assert(Stream.EoF);

	ulong[1] Buffer;
	Stream.Position = 0;
	assert(Stream.Read!ulong(Buffer) == Buffer.length);
	assert(Buffer[0] == Test);

	Stream.Buffer = Test2;
	assert(Stream.Read!ulong(Buffer) == Buffer.length);

	version(BigEndian)
	{
		assert(Buffer[0] == Test);
	}
	else
	{
		assert(Buffer[0] == 0xEFCDAB8967452301);
	}

	Stream.Position = 0;

	auto Stream2 = new MemoryStream(Test2.length, false);
	Stream.CopyTo(Stream2);

	void TestStream2()
	{
		Stream2.Position = 0;

		ubyte[] Temp;
		Temp.length = Test2.length;
		assert(Stream2.Read!ubyte(Temp) == Temp.length);
		assert(Temp == Test2);
	}
	TestStream2();

	Stream2.Clear();
	Stream.CopyTo(Stream2, Stream.Length - 1);
	TestStream2();

	Stream.Clear();
	Stream.WriteByte(0x02);
	Stream.Position = 0;
	assert(Stream.Peek() == 0x02);
	assert(Stream.ReadByte() == 0x02);

	Stream.Seek(2, SeekOrigin.Current);
	assert(Stream.Position == 3);

	Stream.Seek(0, SeekOrigin.End);
	assert(Stream.Position == (Stream.Length - 1));

	Stream.Clear();
	Stream.Write!ubyte(Test2 ~ 0xF4);
	Stream.Position = 0;

//	writeln(Stream.Buffer);
//	writeln(Stream.Length);
//	readln();

	ushort[5] Shorts;
	assert(Stream.Read(Shorts) == 4);
	assert(Shorts[4] == 0x00); // Since only 4 were read in, this one shouldn't have a value

	Stream.Dispose(); assert(Stream.IsDisposed);

	Stream = new MemoryStream(6, false);

	uint[1] BufferInt = [0xABCDEF01];
	Stream.Write(BufferInt);

	BufferInt[0] = 0x224488AA;
	Stream.Write(BufferInt);

	version(BigEndian)
	{
		assert(Stream.Buffer == [0xAB, 0xCD, 0xEF, 0x01, 0x22, 0x44]);
	}
	else
	{
		assert(Stream.Buffer == [0x01, 0xEF, 0xCD, 0xAB, 0xAA, 0x88]);
	}
}