﻿module System.IO.Path;

private
{
	import std.traits, std.path, std.exception;
}

public
{	
	import System.ArgumentNullException;
}

// TODO: The rest of the functions that I can manage right now.
// TODO: Fix this up so it works for Linux(When I ran unittests, this was the only class that failed)
/// A class that provides static functions to manipulate path strings.
static class Path
{
	@safe
	public static pure
	{
		/++
		 + 	Changes the extension of a path string.
		 + 
		 + Parameters:
		 + 	path = The path to change the extension of.
		 + 	extension = The new extension(without the dot).
		 + 
		 + 
		 + Example:
		 + --
		 + 	import System.IO.Path;
		 + 
		 + 	void Foo()
		 + 	{
		 + 		assert(Path.ChangeExtention("CatImage.jpg", "png") == "CatImage.png");
		 + 	}
		 + --
		 + 
		 + Returns:
		 + 	If $(B path) is a null or empty("") string then an emtpy string is returned.
		 + 	If $(B path) doesn't contain an extension then $(B exension) is appended to it.
		 + 	If $(B extension) is null then $(B path) is returned without an extension.
		 + 	Otherwise, $(B path) is returned with $(B extension) replacing it's old extension.
		 + 
		 + +/
		T ChangeExtension(T)(in T path, in T extension)
		if(isSomeString!(T))
		{
			if(path is null || path == "")
			{
				return "";
			}
			else if(extension is null || extension == "") // Clear extension
			{
				return Path.StripExtension(path);
			}
			else if(!Path.HasExtension(path)) // If there's no Extension then add one
			{
				return (path ~ "." ~ extension);
			}
			else // Actually changing it
			{
				return (Path.StripExtension(path) ~ "." ~ extension);
			}
		}

		/++
		 + 	Combines multiple paths together into a single path string. Parses ".." in paths.
		 + 	If any of the paths are null or empty then they are skipped over.
		 +  If an absolute path is passed to the function, then any previous progess will be scrapped and the given absolute path will take it's place.
		 + 
		 + Parameters:
		 + 	paths = The paths to combine.
		 + 
		 + 
		 + Example:
		 + ---
		 + 	import System.IO.Path;
		 + 
		 + 	void Foo()
		 + 	{
		 + 		string[] Paths = ["C:", "User", "Cat lady", "Cat images"];
		 + 		assert(Path.Combine(Paths) == "C:\\User\\Cat lady\\Cat images\\");
	 	 +	} 
		 + ---
		 + 
		 + Returns:
		 + 	All of the path strings in $(B path) combined into a single path string. 
		 + +/
		T Combine(T)(in T[] paths)
		if(isSomeString!(T))
		{
			T ToReturn = "";

			bool HasSlash(T input)
			{
				return (input[$ - 1] == '\\' || input[$ - 1] == '/');
			}

			foreach(i, path; paths)
			{
				void AddSlash()
				{
					version(Windows)
					{
						ToReturn ~= (path ~ '\\');
					}
					else
					{
						ToReturn ~= (path ~ '/');
					}
				}

				if(path is null || path.length == 0) // Skip any usless paths
				{
					continue;
				}
				else if(path.isAbsolute())  // As stated in the .Net version, when an absolute path is reached then I must start over again.
				{
					ToReturn = path;

					if(!HasSlash(path))
					{
						version(Windows)
						{
							ToReturn ~= '\\';
						}
						else
						{
							ToReturn ~= '/';
						}
					}
				}
				else
				{
					if(!HasSlash(path) || i == (paths.length - 1)) // If the thing doesn't have a slash, or if it's the last path.
					{
						if(Path.HasExtension(path) && i == (paths.length - 1)) // If the last path has an extention, just add it to the output.
						{
							ToReturn ~= path;
							continue;
						}

						// Add the slash into the end of that path
						AddSlash();
					} // Otherwise, add the already-slashed thing into the path.
					else
					{
						ToReturn ~= path;
					}
				}
			}

			return ToReturn;
		}

		/++
		 + 	Returns the extension of a path string(without the dot).
		 + 
		 + Parameters:
		 + 	path = The path string to get the extension of.
		 + 
		 + 
		 + Throws $(B System.ArgumentException) if $(B path) is null.
		 + 
		 + 
		 + Example:
		 + ---
		 + 	import System.IO.Path;
		 + 	
		 + 	void Foo()
		 + 	{
		 + 		assert(Path.GetExtension("Images/Mr Fluffypaws.png") == "png");
		 + 	}
		 + ---
		 + 
		 + Returns:
		 + 	If $(B path) doesn't contain an extension or is null, then an empty string("") is returned.
		 + 	Otherwise $(B path)'s extension is returned.
		 + +/
		T GetExtension(T)(in T path)
		if(isSomeString!(T))
		{
			enforce(path !is null, new ArgumentNullException("path"));

			size_t x = path.length;

			foreach_reverse(chr; path)
			{
				// If there's no Extension
				if(chr == '\\' || chr == '/')
				{
					break;
				}
				// If there is an Extension
				else if(chr != '.')
				{
					x -= 1;
				}
				else
				{
					return path[x..$];
				}
			}

			return ""; // No Extension
		}

		/// Gets the absolute path for a given path string, supports "..".
		/// 
		/// Parameters:
		/// 	path = The path string to get the absolute path for.
		/// 
		/// 
		/// Throws $(B System.ArgumentException) if $(B path) is zero-lengthed or null.
		/// 
		/// Example:
		/// ---
		/// 	import System.IO.Path;
		/// 
		/// 	void Foo()
		/// 	{
		/// 		// Imagine the current working directory is C:/Program Files/My App/
		/// 		assert(Path.GetFullPath("../../Compilers/D" == "C:\\Compilers\\D\\"));
		/// 	}
		/// ---
		/// 
		/// Returns:
		/// 	The absolute path for $(B path).
		T GetFullPath(T)(in T path)
		if(isSomeString!(T))
		{
			import std.conv;

			enforce(path !is null && path.length > 0, new ArgumentException("The given path must not be null or zero-lengthed.", "path"));

			T[] Tree;
			T Absolute = to!(T)(absolutePath(path)); // Annoyingly, this function doesn't handle ".." (At least not when I tested it, also tested with ".", "...", and "....")
			T Buffer = "";

			// This function handles any ".." things in the path.
			void HandleDots()
			{
				// Make sure there's something in the buffer, and the only thing there is a ".."
				if(Buffer != "")
				{
					if(Buffer == "..")
					{
						// Remove the last thing in the tree if there is anything there.
						if(Tree.length > 0)
						{
							Tree.length -= 1;
						}
					}
					else
					{
						Tree ~= Buffer;
					}
				}
			}

			// Go over each character in the absolute path
			for(size_t i = 0; i < Absolute.length; i++)
			{
				auto Char = Absolute[i];

				// If we hit a slash, then attempt to handle "..".
				if(Char == '\\' || Char == '/')
				{
					HandleDots();

					Buffer = "";
				}
				else // Otherwise just add the character to the buffer.
				{
					Buffer ~= Char;
				}
			}
			if(Buffer != "") HandleDots(); // If there's still things in the buffer, handle ".." or add it to the tree.

			return Path.Combine(Tree);
		}

		// Extra method - not in .Net
		/++
		 + 	Removes the extension from a path string.
		 + 
		 + Parameters:
		 + 	path = The path string to remove the extension from.
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B path) is null.
		 + 
		 + 
		 + Example:
		 + ---
		 + 	import System.IO.Path;
		 + 
		 + 	void Foo()
		 + 	{
		 + 		assert(Path.StripExtension("Sir Mittens.png.exe" == "Sir Mittens.png"));
		 + 	}
		 + ---
		 + 
		 + Returns:
		 + 	$(B path) without an extension.
		 + +/
		T StripExtension(T)(in T path)
		if(isSomeString!(T))
		{
			enforce(path !is null, new ArgumentNullException("path"));

			size_t ExtensionLength = Path.GetExtension(path).length;
			ExtensionLength += (ExtensionLength > 0) ? 1 : 0; // We add 1 so the line below also gets rid of the ".", and we don't add 1 when there's no extension so the line below doesn't randomly cut off a single character from the end.

			return path[0..($ - ExtensionLength)]; // Should I be .dup ing this?
		}

		/++
		 + Determines whether a path string contains an extension.
		 + 
		 + Parameters:
		 + 	path = The path to check for an extension.
		 + 
		 + 
		 + Example:
		 + ---
		 + 	import System.IO.Path;
		 + 
		 + 	void Foo()
		 + 	{
		 + 		assert(Path.HasExtension("Melon Cat.png"));
		 + 	}
		 + ---
		 +  
		 + Returns:
		 + 	If $(B path) contains an extension, then $(B true) is returned.
		 + 	Otherwise, $(B false) is returned.
		 + +/
		bool HasExtension(T)(in T path)
		if(isSomeString!(T))
		{
			return (Path.GetExtension(path) != "");
		}

		/// Returns the directory part of the path.
		/// Or to describe it another way - Cut off the right-most file/directory from the path.
		/// 
		/// Parameters:
		/// 	path = The path to get the directory of.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B path) is null.
		/// 
		/// 
		/// Example:
		/// ---
		/// 	import System.IO.Path;
		/// 
		/// 	void Foo()
		/// 	{
		/// 		assert(Path.GetDirectoryName("C:\\Program Files\\SomeProgram\\bin") == "C:\\Program Files\\SomeProgram\\")
		/// 	}
		/// ---
		/// 
		/// Returns:
		/// 	The directory part of the path.
		T GetDirectoryName(T)(in T path)
		if(isSomeString!T)
		{
			enforce(path !is null, new ArgumentNullException("path"));

			size_t Amount = 0;

			// Find where the first slash is, then slice to it
			foreach_reverse(ch; path)
			{
				// We check for Amount > 0 because we want to skip the slash if it's the very last character.
				if((ch == '\\' || ch == '/') && Amount > 0)
				{
					break;
				}
				else
				{
					Amount += 1;
				}
			}

			return path[0..$ - Amount];
		}
	}
}
unittest
{
	// If ChangeExtension works for all of it's cases, then a lot of the other extension functions also work as an indirect result. <3
	string SPath = "Programming/C+#Minus";
	string SExtension = "png";
	string FPath = "Amazing folder/Super sub-folder/..";

	// Don't question the values... Thinking stuff up on the spot is hard(At least the rest of the unittests are clean, mostly)
	dstring DPath = "Cat images/IMAGE_1020203_Mr_Fluffykins";
	dstring DExtension = "png";

	assert(Path.ChangeExtension!string(null, null) == ""); // Empty/null path check
	assert(Path.ChangeExtension(SPath, SExtension) == (SPath ~ "." ~ SExtension)); // No extention
	assert(Path.ChangeExtension(DPath, DExtension) == (DPath ~ "."d ~ DExtension)); // ^^
	assert(Path.ChangeExtension(SPath, null)); // Empty/null extension check
	assert(Path.ChangeExtension(SPath ~ "." ~ SExtension, "mp4") == (SPath ~ ".mp4")); // Changing path
	assert(Path.ChangeExtension(DPath ~ "." ~ DExtension, "mp4"d) == (DPath ~ ".mp4"d)); // ^^

	version(Windows)
	{
		assert(Path.Combine!string([SPath, SExtension]) == (SPath ~ "\\" ~ SExtension ~ "\\"), Path.Combine!string([SPath, SExtension]));
		assert(Path.Combine(["Some folder", "", null, "Other folder"]) == "Some folder\\Other folder\\", Path.Combine(["Some folder", "", null, "Other folder"]));
		assert(Path.Combine(["Some folder", "Other folder", "C:\\Windows", "System32\\", "super-important.dll"]) == "C:\\Windows\\System32\\super-important.dll", Path.Combine(["Some folder", "Other folder", "C:\\Windows", "System32\\", "super-important.dll"]));
		assert(Path.GetFullPath(FPath)[$ - 15..$] == "Amazing folder\\", Path.GetFullPath(FPath)[$ - 15..$]); // "Amazing folder/".length == 15, I just want to make sure this is on the end
		
		// It works <3
		// Before you ask, anything under "Jaster_Collection" are libraries that I make for myself to use, as I may have use for them(I just decided to give them a cringey name).
		//assert(Path.GetFullPath("../" ~ SPath) == "G:\\Programming\\D\\DLL\\Jaster_Collection\\Programming\\C+#Minus\\", Path.GetFullPath("../" ~ SPath));
	}
	else
	{
		assert(Path.Combine!string([SPath, SExtension]) == (SPath ~ "/" ~ SExtension ~ "/"), Path.Combine!string([SPath, SExtension]));
		assert(Path.Combine(["Some folder", "", null, "Other folder"]) == "Some folder/Other folder");
		assert(Path.GetFullPath(FPath)[$ - 15..$] == "Amazing folder//"); // "Amazing folder/".length == 16, I just want to make sure this is on the end
	}

	assert(Path.GetDirectoryName("C:/Program Files/SomeProgram/bin") == "C:/Program Files/SomeProgram/", Path.GetDirectoryName("C:/Program Files/SomeProgram/bin"));
	assert(Path.GetDirectoryName("/var/") == "/", Path.GetDirectoryName("/var/"));
}