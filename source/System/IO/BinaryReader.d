﻿module System.IO.BinaryReader;

private
{
	import std.exception;
	import std.traits : isArray, isSomeString;
}

public
{
	import System.ByteOrder, System.IO.Stream, System.IDisposable,
		System.ArgumentNullException, System.ObjectDisposedException, System.BitConverter;
}

/// A class to aid with reading in binary data from a stream, and converting the bytes read, intto other types.
class BinaryReader(T : Stream) : IDisposable
{
	private
	{
		T			_Base;
		ByteOrder	_DesiredOrder;
		
		bool		_IsDisposed;
		bool		_CloseStream;
	}
	
	public
	{
		/++
		 + 	Constructs a new BinaryReader that gets it's input from $(B stream).
		 + 	If specified, the BinaryReader will call $(B stream)'s Dispose function once the BinaryReader has been disposed of/deconstructed.
		 + 
		 + Parameters:
		 + 	stream = The stream to read the BinaryReader's input from. Must support reading.
		 + 	closeWhenDone = If true, $(B stream) is closed when this current BinaryReader object is disposed/destroyed. False prevents this behaviour
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B stream) is null.
		 + Throws $(B System.InvalidOperationException) if $(B stream).CanRead is false.
		 + Throws $(B System.ObjectDisposedException) if $(B stream).IsDisposed is true.
		 + +/
		this(T stream, bool closeWhenDone = false)
		{
			enforce(stream !is null,	new ArgumentNullException("stream"));
			enforce(stream.CanRead,		new InvalidOperationException("The given stream does not support reading."));
			enforce(!stream.IsDisposed,	new ObjectDisposedException("The given stream has been disposed."));
			
			this._Base 			= stream;
			this._CloseStream 	= closeWhenDone;
			this._IsDisposed	= false;
			this._DesiredOrder  = ByteOrder.Native;
		}
		
		~this()
		{
			this.Dispose();
		}
		
		// Convinence method.
		/++
		 + 	Reads a certain amount of data from the BinaryReader's base stream in and converts it to a desired byte order.
		 +.	
		 + 	If the BinaryReader's Order is ByteOrder.Big, and ByteOrder.Native is ByteOrder.Little, a conversion takes place.
		 + 	If the BinaryReader's Order is ByteOrder.Little, and ByteOrder.Native it ByteOrder.Big, a conversion takes place.
		 + 	If the BinaryReader's Order is the same as ByteOrder.Native, then no conversion takes place.
		 + 
		 + Parameters:
		 + 	amount = How many objects of $(B T) to read in.
		 + 
		 + Returns:
		 + 	$(B amount) amount of $(B T) read in from the BinaryReader's base stream.
		 + +/
		T Read(T)()
		if(!is(T == class) && !isArray!T && !isSomeString!T)
		{
			return this.Read!T(1)[0];
		}
		
		/// ditto
		T[] Read(T)(size_t amount)
		if(!is(T == class) && !isSomeString!T)
		{			
			T[] ToReturn;
			ToReturn.length = amount;
			this._Base.Read!T(ToReturn);

			if(this._DesiredOrder != ByteOrder.Native)
			{
				BitConverter.SwapOrder(ToReturn);
			}

			return ToReturn;
		}
		
		// TODO: Add support to choose how long the length-prefix is.
		/++
		 + 	Read in a length-prefixed string from the BinaryReader's stream.
		 + 	The length-prefix is an unsigned 16-bit integer.
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B data) is null.
		 + +/
		T Read(T)()
		if(isSomeString!T)
		{			
			static if(is(T == string))
			{
				char[] Buffer;
			}
			else if(is(T == dstring))
			{
				dchar[] Buffer;
			}
			else
			{
				wchar[] Buffer;
			}
			Buffer.length = this.Read!ushort();

			this._Base.Read!(typeof(Buffer[0]))(Buffer);
			return cast(T)Buffer;
		}
		
		/// Releases the BinaryReader's resources.
		void Dispose()
		{
			if(!this.IsDisposed)
			{
				if(this._CloseStream)
				{
					this._Base.Dispose();
				}
				
				this._IsDisposed = true;
			}
		}
		
		/// Returns whether the BinaryReader has been disposed yet.
		@property
		bool IsDisposed()
		{
			return this._IsDisposed;
		}
		
		/// Gets the BinaryReader's output stream.
		@property
		T BaseStream()
		{
			return this._Base;
		}
		
		/// Set the BinaryReader's desired byte order.
		@property
		void Order(ByteOrder order)
		{
			this._DesiredOrder = order;
		}
		
		/// Get the BinaryReader's desired byte order.
		@property
		ByteOrder Order()
		{
			return this._DesiredOrder;
		}
		
		/// Seek to a position in the stream.
		alias Seek = this._Base.Seek;
	}
}
unittest
{
	// The unittest for this class is in the BinaryWriter unittest
}