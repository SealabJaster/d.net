﻿module System.IO.BinaryIO;

private
{
	import std.exception;
}

public
{
	import System.IO.Stream, System.IO.BinaryWriter, System.IO.BinaryReader,
		System.ArgumentNullException, System.IDisposable, System.InvalidOperationException;
}

/// Convinence class used to provide a BinaryWriter and BinaryReader for the same stream.
class BinaryIO(T : Stream) : IDisposable
{
	private
	{
		BinaryWriter!T	_Writer;
		BinaryReader!T	_Reader;

		bool			_Disposed = false;
	}

	public
	{
		/// Constructs a new $(B BinaryIO) object that creates a $(B BinaryReader) and $(B BinaryWriter) for the given stream.
		/// If the given stream does not support reading, then the BinaryIO's Reader will be null.
		/// If the given stream does not support writing, then the BinaryIO's Writer will be null.
		/// 
		/// Parameters:
		/// 	stream = The stream the BinaryWriter and BinaryReader shall use.
		/// 	closeWhenDone = Should the stream be closed once Dispose has been called.
		/// 
		/// 
		/// Thros $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(I [See: BinaryWriter and BinaryReader's constructors])
		this(T stream, bool closeWhenDone)
		{
			enforce(stream !is null, new ArgumentNullException("stream"));

			if(stream.CanRead)	this._Writer = new BinaryWriter!T(stream, closeWhenDone);
			if(stream.CanWrite)	this._Reader = new BinaryReader!T(stream, closeWhenDone);
		}

		/// Disposes the BinaryIO's Writer and Reader, and optionally the stream being accessed by them.
		void Dispose()
		{
			if(this._Writer !is null) this._Writer.Dispose();
			if(this._Reader !is null) this._Reader.Dispose();

			this._Disposed = true;
		}

		/// Get whether the BinaryIO has been disposed.
		@property
		bool IsDisposed()
		{
			return this._Disposed;
		}

		/// Get the BinaryIO's BinaryWriter object.
		@property
		BinaryWriter!T Writer()
		{
			return this._Writer;
		}

		/// Get the BibayrIO's BinaryReader object.
		@property
		BinaryReader!T Reader()
		{
			return this._Reader;
		}
	}
}

// TODO: Unittests