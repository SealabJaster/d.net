﻿module System.IO.TextReader;
// Slight warning for people expecting an exact(ish) copy of .Net, I really didn't like the .Net TextReader, so I've changed it to my liking (Which might be a very bad thing...)
private
{
	import std.traits, std.exception;
}

public
{
	import System.ArgumentNullException, System.InvalidOperationException, System.IDisposable,
		System.ObjectDisposedException, System.IO.EndOfStreamException;
}

/// A class that aids in reading data from strings.
class TextReader(T) : IDisposable
if(isSomeChar!T)
{
	private
	{
		String	_Buffer;
		size_t	_Position;

		alias 	Char 	= T;
		alias 	String 	= immutable(Char)[];

		static String _Blanks = ['\t', '\r', '\n', ' '];

		bool	_Disposed;

		void ThrowIfDisposed()
		{
			enforce(!this._Disposed, new ObjectDisposedException("Attempted to read after disposing the TextReader."));
		}
	}

	public
	{
		/// Constructs a new TextReader that uses a given buffer as it's data.
		/// 
		/// Parameters:
		/// 	buffer = The data that the TextReader shall use.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B buffer) is null.
		this(String buffer)
		{
			enforce(buffer !is null, new ArgumentNullException("buffer"));

			this._Buffer = buffer;
		}

		/// Releases the TextReader's reference to it's buffer.
		void Dispose()
		{
			this._Disposed = true;
			this._Buffer = null;
		}

		/// Returns the next character in the TextReader's buffer.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// 
		/// Returns:
		/// 	The next character in the TextReader's buffer.
		Char Peek()
		{
			this.ThrowIfDisposed();
			enforce(!this.EoF, new EndOfStreamException("read"));

			return this._Buffer[this._Position];
		}

		/// Reads a given amount of characters from the TextReader's buffer and returns them.
		/// May return less characters than wanted.
		/// The returned data is simply a slice from the TextReader's buffer, keep this in mind if you want to edit it's data.
		/// 
		/// Parameters:
		/// 	amount = The amount of characters to read.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true before any reading attempt.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// 
		/// Returns:
		/// 	$(B amount) amount of characters if EoF is not hit during reading.
		/// 	Otherwise, if EoF is hit, then less than $(B amount) amount of characters.
		String Read(scope size_t amount)
		{
			this.ThrowIfDisposed();
			enforce(!this.EoF, new EndOfStreamException("read"));

			// Make sure we stay in bounds
			if((this.Position + amount) >= this._Buffer.length)
			{
				amount = (this._Buffer.length - this.Position);
			}

			// This will probably be amazingly slow when I add the option to parse escape characters
			this._Position += amount;
			return this._Buffer[this.Position - amount..this.Position]; // Should I be duping this?
		}

		/// Reads characters from the TextReader's buffer until either the end of the stream is reached or until one of the characters contained in a given delimeter is found.
		/// 
		/// Parameters:
		/// 	delimeters = An array of characters that will stop this function from reading any more characters.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true before any reading attempt.
		/// Throws $(B System.ArgumentNullException) if $(B delimeters) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// 
		/// Returns:
		/// 	An random-lengthed string.
		String ReadTo(in String delimeters)
		{
			enforce(delimeters !is null, new ArgumentNullException("delimeters"));

			size_t 	Amount = 0;
			bool 	Loop = true;

			// Keep checking characters until we hit EoF or a delimeter, then read in the characters that weren't a delimeter
			while(!this.EoF && Loop)
			{
				auto Next = this.Peek();

				foreach(ch; delimeters)
				{
					if(Next == ch)
					{
						Loop = false;
						break;
					}
				}
				if(!Loop) break;

				Amount += 1;
				this._Position += 1;
			}

			this._Position -= Amount;
			return this.Read(Amount);
		}

		/// Reads in a string that contains only digits, ".", and ",".
		/// 
		/// Returns:
		/// 	A string containing a valid number(Hopefully).
		String ReadNumberString()
		{
			size_t 	Amount = 0;

			while(!this.EoF)
			{
				auto Next = this.ReadChar();

				if((Next >= '0' && Next <= '9') || Next == '.' || Next == ',')
				{
					Amount += 1;
				}
				else
				{
					this._Position -= 1;
					break;
				}
			}

			this._Position -= Amount;
			return this.Read(Amount);
		}

		/// Reads a single line from the TextReader's buffer.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true before any reading attempt.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// 
		/// Returns:
		/// 	A single line.
		String ReadLine()
		{
			auto A = this.ReadTo(['\r', '\n']);
			this.SkipBlanks();

			return A;
		}

		/// Reads a single character from the TextReader's buffer.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true before any reading attempt.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// 
		/// Returns:
		/// 	A single character.
		Char ReadChar()
		{
			return this.Read(1)[0];
		}

		/// Moves the Stream's Position forward until a non-blank character has been found.
		/// Non-blank characters inlcude - '\t', '\r', '\n', and ' '
		///
		///
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		void SkipBlanks()
		{
			this.ThrowIfDisposed();

			while(!this.EoF)
			{
				auto Next = this.Peek();
				bool Break = true;

				foreach(ch; this._Blanks)
				{
					if(Next == ch)
					{
						this._Position += 1;
						Break = false;
					}
				}

				if(Break)
				{
					break;
				}
			}
		}

		/// Reads every character from the TextReader's current position to the end of it's buffer.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B this.EoF) is true before any reading attempt.
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		///
		/// Returns:
		/// 	Every character from the TextReader's current position to the end of it's buffer.
		String ReadToEnd()
		{
			return this.Read(this._Buffer.length - this._Position);
		}

		/// Get whether the TextReader is at the end of it's buffer
		@property
		bool EoF() // "End of file" doesn't really make sense, but I'm just too used to it to change.
		{
			return (this.Position >= this._Buffer.length);
		}

		/// Get the TextReader's position in it's buffer.
		@property
		size_t Position()
		{
			return this._Position;
		}

		/// Set the TextReader's position in it's buffer.
		@property
		void Position(size_t position)
		{
			this._Position = position;
		}

		/// Get the TextReader's buffer.
		@property
		const(String) Buffer()
		{
			return this._Buffer;
		}

		/// Get whether the TextReader has been disposed.
		@property
		bool IsDisposed()
		{
			return this._Disposed;
		}
	}
}
unittest
{
	// This class' unittest is in TextWriter's unittest
}