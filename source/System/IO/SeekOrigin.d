﻿module System.IO.SeekOrigin;

private
{
	import std.stdio : SEEK_CUR, SEEK_END, SEEK_SET;
}

/// Specifies where to start a seeking operation from, for $(B System.IO.Stream) classes.
enum SeekOrigin
{
	/// Start the seek at the beginning at the stream
	Begin = SEEK_SET,

	/// Start the seek from the current position of the stream
	Current = SEEK_CUR,

	/// Start the seek from the end of the stream
	End = SEEK_END
}