﻿module System.IO;

public
{
	import System.IO.Path, System.IO.SeekOrigin, System.IO.Stream,
		System.IO.MemoryStream, System.IO.BinaryReader, System.IO.BinaryWriter, 
		System.IO.BinaryIO, System.IO.TextReader, System.IO.TextWriter,
		System.IO.FileAccess, System.IO.FileMode, System.IO.FileNotFoundException,
		System.IO.FileStream, System.IO.SeekOrigin, System.IO.EndOfStreamException,
		System.IO.IOException, System.IO.BufferedStream;
}