﻿module System.IO.BinaryWriter;

private
{
	import std.exception;
	import std.traits : isArray, isSomeString;
}

public
{
	import System.ByteOrder, System.IO.Stream, System.IDisposable,
		System.ArgumentNullException, System.ObjectDisposedException, System.BitConverter;
}

/// A class to aid in writing out binary data to a stream.
class BinaryWriter(T : Stream) : IDisposable
{
	private
	{
		T			_Base;
		ByteOrder	_DesiredOrder;

		bool		_IsDisposed;
		bool		_CloseStream;
	}

	public
	{
		/++
		 + 	Constructs a new BinaryWriter that writes it's output to $(B stream).
		 + 	If specified, the BinaryWriter will call $(B stream)'s Dispose function once the BinaryWriter has been disposed of/deconstructed.
		 + 
		 + Parameters:
		 + 	stream = The stream to write the BinaryWriter's output to. Must support writing.
		 + 	closeWhenDone = If true, $(B stream) is closed when this current BinaryWriter object is disposed/destroyed. False prevents this behaviour
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B stream) is null.
		 + Throws $(B System.InvalidOperationException) if $(B stream).CanWrite is false.
		 + Throws $(B System.ObjectDisposedException) if $(B stream).IsDisposed is true.
		 + +/
		this(T stream, bool closeWhenDone = false)
		{
			enforce(stream !is null,	new ArgumentNullException("stream"));
			enforce(stream.CanWrite,	new InvalidOperationException("The given stream does not support writing."));
			enforce(!stream.IsDisposed,	new ObjectDisposedException("The given stream has been disposed."));

			this._Base 			= stream;
			this._CloseStream 	= closeWhenDone;
			this._IsDisposed	= false;
			this._DesiredOrder  = ByteOrder.Native;
		}

		~this()
		{
			this.Dispose();
		}

		// Convinence method.
		/++
		 + 	Writes a given piece of data to the BinaryWriter's base stream in a desired byte order.
		 +.	
		 + 	If the BinaryWriter's Order is ByteOrder.Big, and ByteOrder.Native is ByteOrder.Little, a conversion takes place.
		 + 	If the BinaryWriter's Order is ByteOrder.Little, and ByteOrder.Native it ByteOrder.Big, a conversion takes place.
		 + 	If the BinaryWriter's Order is the same as ByteOrder.Native, then no conversion takes place.
		 +. 
		 + 	The given data is first duplicated before it is converted, so that the given data remains unchanged after this function is called.
		 + 	No conversion takes place if the type of data is 1 byte in size.
		 + 
		 + Parameters:
		 + 	data = The data to write.
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B data) is null.
		 + +/
		void Write(T)(T data)
		if(!is(T == class) && !isArray!T && !isSomeString!T)
		{
			this.Write!T(cast(T[])[data]);
		}

		/// ditto
		void Write(T)(T[] data)
		if(!is(T == class) && !isSomeString!T)
		{
			enforce(data !is null,	new ArgumentNullException("data"));
			if(data.length == 0) 	return;

			if(this._DesiredOrder != ByteOrder.Native)
			{
				T[] Temp = BitConverter.SwapOrder(data.dup);
				this._Base.Write(Temp);
			}
			else
			{
				this._Base.Write(data);
			}
		}

		// TODO: Add support to choose how long the length-prefix is.
		/++
		 + 	Write out a length-prefixed string to the BinaryWriter's string.
		 + 	The length-prefix is an unsigned 16-bit integer.
		 + 
		 + Parameters:
		 + 	data = The character data to write.
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B data) is null.
		 + +/
		void Write(T)(T data)
		if(isSomeString!T)
		{
			enforce(data !is null,	new ArgumentNullException("data"));

			this.Write!ushort(cast(ushort)data.length);
			this._Base.Write(data);
		}

		/// Releases the BinaryWriter's resources.
		void Dispose()
		{
			if(!this.IsDisposed)
			{
				if(this._CloseStream)
				{
					this._Base.Dispose();
				}

				this._IsDisposed = true;
			}
		}

		/// Returns whether the BinaryWriter has been disposed yet.
		@property
		bool IsDisposed()
		{
			return this._IsDisposed;
		}

		/// Gets the BinaryWriter's output stream.
		@property
		T BaseStream()
		{
			return this._Base;
		}

		/// Set the BinaryWriter's desired byte order.
		@property
		void Order(ByteOrder order)
		{
			this._DesiredOrder = order;
		}

		/// Get the BinaryWriter's desired byte order.
		@property
		ByteOrder Order()
		{
			return this._DesiredOrder;
		}

		/// Seek to a position in the stream.
		alias Seek = this._Base.Seek;
	}
}
unittest
{
	import System.IO.MemoryStream, std.string, System.IO.BinaryReader, System.IO.BinaryIO;

	auto MStream = new MemoryStream(8, false);
	auto IO = new BinaryIO!MemoryStream(MStream, true);

	assert(MStream == IO.Writer.BaseStream);
	assert(MStream == IO.Reader.BaseStream);

	uint TestBig = 0xAABBEEFF;
	uint TestLittle = 0xFFEEBBAA;

	IO.Writer.Order = ByteOrder.Big; assert(IO.Writer.Order == ByteOrder.Big);
	IO.Writer.Write(TestLittle);
	MStream.Position = 0;
	IO.Reader.Order = ByteOrder.Big; assert(IO.Reader.Order == ByteOrder.Big);
	assert(IO.Reader.Read!uint() == TestLittle);

	IO.Writer.Order = ByteOrder.Little;
	IO.Writer.Write(TestBig);
	MStream.Position = uint.sizeof;
	IO.Reader.Order = ByteOrder.Little;
	assert(IO.Reader.Read!uint() == TestBig);

	assert(MStream.Buffer == [0xFF, 0xEE, 0xBB, 0xAA, 0xFF, 0xEE, 0xBB, 0xAA], format("%s", MStream.Buffer));

	IO.Writer.Order = ByteOrder.Big;
	IO.Reader.Order = ByteOrder.Big;
	MStream.Clear(); // Because it can't resize, it's data just sets it's buffer's data to 0
	IO.Writer.Write!string("Soup");
	MStream.Position = 0;

	assert(IO.Reader.Read!string() == "Soup");
	assert(MStream.Buffer == [0x00, 0x04, 'S', 'o', 'u', 'p', 0x00, 0x00], format("%s", MStream.Buffer));

	IO.Dispose();
	assert(IO.IsDisposed && IO.Writer.IsDisposed && IO.Reader.IsDisposed);

	// I'm slightly OCD about the coverage not being at 100%
	auto Reader = new BinaryReader!MemoryStream(new MemoryStream(0, true), true);
	Reader.Dispose();
}