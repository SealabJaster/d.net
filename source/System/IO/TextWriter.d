﻿
module System.IO.TextWriter;

private
{
	import std.traits, std.utf, std.string, std.exception;

	import System.IO.MemoryStream, System.BitConverter;
}

public
{
	import System.ArgumentNullException, System.InvalidOperationException, System.IDisposable,
		System.ObjectDisposedException;
}

/// A class that aids in writing out text forms of data to a Stream.
class TextWriter(S : Stream, T = char) : IDisposable
if(isSomeChar!T)
{
	private
	{
		S		_Stream;

		alias 	Char 	= T;
		alias 	String 	= immutable(Char)[];

		String	_Terminator = "\n";
	}

	public
	{
		/// Constructs a new TextWriter that uses the given stream to write with.
		/// 
		/// Parameters:
		/// 	stream = The stream to write to.
		/// 
		/// 
		/// Throws $(B System.ArgumentNullException) if $(B stream) is null.
		/// Throws $(B System.ObjectDisposedException) if $(B stream) has been disposed.
		/// Throws $(B System.InvalidOperationException) if $(B stream) does not support writing.
		this(S stream)
		{
			enforce(stream !is null, 	new ArgumentNullException("stream"));
			enforce(stream.CanWrite,	new InvalidOperationException("The given stream does not support writing."));
			enforce(!stream.IsDisposed,	new ObjectDisposedException("The given stream has been disposed."));

			this._Stream = stream;
		}

		/// Clears the TextWriter's buffer.
		void Clear()
		{
			this._Stream.Length = 0;
		}

		/// Clears the TextWriter's buffer, and disposes it's internal stream.
		void Dispose()
		{
			this._Stream.Dispose();
		}

		/// Get whether the TextWriter's internal stream has been disposed
		@property
		bool IsDisposed()
		{
			return this._Stream.IsDisposed;
		}

		/// Converts a given piece of data into a string form and writes it to the TextWriter's buffer.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentNullException) if $(B T2) is an array type and $(B data) is null.
		/// 
		/// Parameters:
		/// 	data = The data to write.
		void Write(T2)(T2 data)
		if(!is(T2 == class))
		{
			static if(isArray!T2)
			{
				enforce(data !is null, new ArgumentNullException("data"));
			}

			static if(!isSomeString!T2)
			{
				auto ToConvert = format("%s", data);
			}
			else
			{
				auto ToConvert = data;
			}

			static if(is(T == wchar))
			{
				this._Stream.Write!(immutable(wchar))(toUTF16(ToConvert));
			}
			else if(is(T == dchar))
			{
				this._Stream.Write!(immutable(dchar))(toUTF32(ToConvert));
			}
			else
			{
				this._Stream.Write!(immutable(char))(ToConvert);
			}
		}

		/// Converts a given piece of data into a string form and writes it to the TextWriter's buffer, the TextWriter's line terminator is also written out at the end.
		/// 
		/// 
		/// Throws $(B System.ObjectDisposedException) if $(B this.IsDisposed) is true.
		/// Throws $(B System.ArgumentNullException) if $(B T2) is an array type and $(B data) is null.
		/// 
		/// Parameters:
		/// 	data = The data to write.
		void WriteLine(T2)(T2 data)
		if(!is(T2 == class))
		{
			this.Write(data);
			this.Write(this._Terminator);
		}

		/// Get whether the TextWriter is at the end of it's buffer
		@property
		bool EoF() // "End of file" doesn't really make sense, but I'm just too used to it to change.
		{
			return this._Stream.EoF;
		}
		
		/// Get the TextWriter's position in it's buffer.
		@property
		size_t Position()
		{
			return this._Stream.Position;
		}
		
		/// Set the TextWriter's position in it's buffer.
		@property
		void Position(size_t position)
		{
			this._Stream.Position = position;
		}

		/// Get the string that the TextWriter outputs at the end of WriteLine.
		@property
		String LineTerminator()
		{
			return this._Terminator;
		}

		/// Set the string that the TextWriter outputs at the end of WriteLine.
		@property
		void LineTerminator(String term)
		{
			this._Terminator = term;
		}

		/// Returns the TextWriter's stream that it is using as a buffer.
		@property
		S BaseStream()
		{
			return this._Stream;
		}
	}
}
unittest
{
	import System.IO.TextReader;

	struct A
	{
		int B;
		string C;
	}

	auto Buffer = new MemoryStream(0, true);
	auto Writer = new TextWriter!MemoryStream(Buffer);
	Writer.LineTerminator = "\t\r\n";
	Writer.WriteLine("Test:");
	Writer.Write(' ');
	Writer.Write("Test");
	Writer.WriteLine("Toob");
	Writer.WriteLine(A(253, "Dabiel"));
	Writer.WriteLine(596.2040);
	Writer.Write("Boot");

	assert(Writer.EoF);
	assert(Writer.LineTerminator == "\t\r\n");

	char[] Buffer2;
	Buffer2.length = Buffer.Length;
	Buffer.Position = 0;

	assert(Buffer.Read!char(Buffer2) == Buffer2.length);
	assert(Buffer2 == "Test:\t\r\n TestToob\t\r\nA(253, \"Dabiel\")\t\r\n596.204\t\r\nBoot", cast(string)Buffer2);
	//assert(Writer.Text == "Test:\t\r\n TestToob\t\r\nBoot");

	auto Reader = new TextReader!char(cast(string)Buffer2);
	assert(Reader.ReadTo(":") == "Test");
	assert(Reader.Position == 4);
	assert(Reader.ReadChar() == ':');
	
	Reader.SkipBlanks();
	auto Temp = Reader.Read(4);
	assert(Temp == "Test", Temp);

	Temp = Reader.ReadLine();
	assert(Temp == "Toob\t", Temp);

	Reader.SkipBlanks();
	assert(Reader.ReadTo("\t\r\n") == "A(253, \"Dabiel\")"); Reader.SkipBlanks();
	assert(Reader.ReadNumberString() == "596.204");
	assert(Reader.ReadToEnd() == "\t\r\nBoot");

	auto X = Reader.Buffer;
	Reader.Position = 0;

	// Pleasing the Code Coverage gods
	Writer.Position = 0;
	assert(Writer.Position == 0);

	Writer.Clear();

	Writer.Dispose(); assert(Writer.IsDisposed);
	Reader.Dispose(); assert(Reader.IsDisposed);

	assert(Writer.BaseStream.IsDisposed);
}