﻿module System.IO.FileMode;

/// Specifies how to open a file.
enum FileMode : ubyte
{
	/// Opens a file if it exists and seeks to the end of the file, or creates a new file. $(B FileMode.Append) can only be used along-side $(B FileAccess.Write). Tryting to seek to a position before the end of the file causes a $(B System.IO.IOException), and any attempt to read causes a $(B System.NotSupportedException).
	Append = 1 << 0,

	/// Create a file if one doesn't already exist. If a file does exist then it is truncated.
	Create = 1 << 1,

	/// Craete a file if one doesn't already exist, If a file exists then a $(B System.IO.IOException) is thrown.
	CreateNew = 1 << 2,

	/// Open an existing file. $(B System.IO.FileAccess) specifies what operations can be performed on the file.
	Open = 1 << 3,

	/// Opens an exist file, creating one if a file does not already exist.
	OpenOrCreate = 1 << 4,

	/// Set the size of an opened file to 0 bytes. Any attempt to read from the file causes a $(B System.NotSupportedException) to be thrown.
	Truncate = 1 << 5
}