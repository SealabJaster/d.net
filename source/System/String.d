﻿module System.String;

/*
 * I love this module so much <3
 * 
 * I think it's probably the best one I've made, in terms of commenting, and the code being beautiful to my eyes <3_<3
 * 
 * */

private
{
	import std.traits : isSomeString, isSomeChar; 
	import std.exception : enforce;
	import std.string : format;
	import std.utf;
}

public
{
	import System.ArgumentNullException, System.FormatException, System.ArgumentOutOfRangeException;
}

// I'd rather have UFCS for these functions, rather than a static class. (It makes things slightly inconsistent though ;_;)

/// Determines whether the given input contains a specified value.
/// 
/// Parameters:
/// 	input = The string to search through.
/// 	value = The string to search for.
/// 
/// 
/// Throws $(B System.ArugmentNullException) if either argument is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("I love dlang".Contains("love dlang"));
/// 	}
/// ---
/// 
/// Returns:
/// 	True if $(B input) contains $(B value).
@safe
bool Contains(T)(in T input, in T value) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(value !is null, new ArgumentNullException("value"));

	// Make sure we're actually looking for something, and the thing we're looking for fits into the input.
	if(value.length == 0 || input.length < value.length)
	{
		return false;
	}

	// Here we just calculate how many characters we can start checking "value" from before "value" no longer fits into "input".
	// We do this so we don't have to have some sort of bounds checking within the loop below, so it's slightly faster?(Maybe.. I hope)
	/*
	 * input = "Foxy!"(6)
	 * value = "Fox"(3)
	 * 
	 * Length = ((6 - 3) + 1) = 4 
	 * */
	size_t Length = ((input.length - value.length) + 1);
	size_t Count = 0;

	while(Count < Length)
	{
		if(input[Count] == value[0])
		{
			// Once we find the first character of the value we want, get a slice equal to the length of value, and then see if they match up.
			if(input[Count..Count + value.length] == value)
			{
				return true;
			}
		}

		Count += 1;
	}

	return false;
}

/// Determines whether the given input ends with a specified value.
/// 
/// Parameters:
/// 	input = The string to search through.
/// 	value = The string to search for.
/// 
/// 
/// Throws $(B System.ArugmentNullException) if either argument is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("I love dlang".EndsWith("lang"));
/// 	}
/// ---
/// 
/// Returns:
/// 	True if $(B input) ends with $(B value).
@safe
bool EndsWith(T)(in T input, in T value) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(value !is null, new ArgumentNullException("value"));

	if(value.length == 0 || input.length < value.length)
	{
		return false;
	}

	return (input[$ - value.length..$] == value);
}

/// Get the index of the first occurance of a specified value in a given input.
/// 
/// Parameters:
/// 	input = The string to search through.
/// 	value = The string to search for.
/// 
/// 
/// Throws $(B System.ArgumentNullException if either argument is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		auto Text = "I love dlang";
/// 		auto Index = Text.IndexOf(" ");
/// 
/// 		assert(Index == 2);
/// 		assert(Text[Index..$].IndexOf(" ") == 7);
/// 	}
/// ---
/// 
/// Returns:
/// 	A zero-based index of where $(B value) first occurs in $(B input) where the index is the position of the occurance's first character. -1 is returned if $(B value) is bigger than the given input, or if $(B input) does not contain $(B value).
@safe
ptrdiff_t IndexOf(T)(in T input, in T value) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(value !is null, new ArgumentNullException("value"));

	if(value.length == 0 || input.length < value.length)
	{
		return -1;
	}

	// See the Contains function for what this does.
	ptrdiff_t Length = ((input.length - value.length) + 1);
	ptrdiff_t Count = 0;

	while(Count < Length)
	{
		if(input[Count] == value[0])
		{
			if(input[Count..Count + value.length] == value)
			{
				return Count;
			}
		}
		
		Count += 1;
	}
	
	return -1;
}

/// Get the index of the first occurance of a specified range of value in a given input.
/// 
/// Parameters:
/// 	input = The string to search through.
/// 	values = The strings to search for.
/// 
/// 
/// Throws $(B System.ArgumentNullException if either argument is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		string	Text 	= "I love dlang";
/// 		string[] Delims = ["I", "love", "dlang"];
/// 
/// 		assert(Text.IndexOfAny(Delims) 			== 0);
/// 		assert(Text[1..$].IndexOfAny(Delims) 	== 3);
/// 		assert(Text[4..$].IndexOfAny(Delims) 	== 8);
/// 	}
/// ---
/// 
/// Returns:
/// 	A zero-based index of where any string in $(B values) first occurs in $(B input) where the index is the position of the occurance's first character. -1 is returned if $(B input) does not contain anything in $(B values).
@safe
ptrdiff_t IndexOfAny(T)(in T input, in T[] values) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(values !is null, new ArgumentNullException("values"));

	// If this is false, then just return -1 since none of the values will work
	bool AtLeastOneWillWork = false;

	// Filter out any "Guarenteed to be -1" values, since it's pretty pointless to do anything with them
	T[] Filter;
	Filter.length = values.length;

	ptrdiff_t SmallestLength = ptrdiff_t.max; // This is used after the loop to calculate "Length"
	ptrdiff_t Count = 0;

	// Should I even be bothering with this?
	foreach(value; values)
	{
		// If the value to check is null, empty, or is larger than the input, then skip past it.
		if(value is null || value.length == 0 || input.length < value.length)
		{
			continue;
		}
		else
		{
			// We keep track of the smallest length, so we can stop the function early -- when there's not enough characters left to read for any of the values.
			if(value.length < SmallestLength)
			{
				SmallestLength = value.length;
			}

			Filter[Count++] 	= value;
			AtLeastOneWillWork 	= true;
		}
	}
	Filter.length = Count; // Resize the slice so it only has the right strings

	if(!AtLeastOneWillWork)
	{
		return -1;
	}

	// See the Contains function for what this does.
	ptrdiff_t Length = ((input.length - SmallestLength) + 1);
	Count = 0;

	while(Count < Length)
	{
		for(uint i = 0; i < Filter.length; i++)
		{
			auto value = Filter[i];

			// Don't bother with values that need more characters than that are left, otherwise crashy things can happen.
			if((value.length + Count) >= input.length)
			{
				continue;
			}

			if(input[Count] == value[0])
			{
				if(input[Count..Count + value.length] == value)
				{
					return Count;
				}
			}
		}
		
		Count += 1;
	}

	return -1;
}

/// Determines whether the given input starts with a specified value.
/// 
/// Parmeters:
/// 	input = The string to search through.
/// 	value = The string to search for.
/// 
/// 
/// Throws $(B System.ArgumentNullException if either argument is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 	
/// 	void Foo()
/// 	{
/// 		assert("I love dlang".StartsWith("I lov"));
/// 	}
/// ---
/// 
/// Returns:
/// 	True if $(B input) starts with $(B value).
@safe
bool StartsWith(T)(in T input, in T value) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(value !is null, new ArgumentNullException("value"));

	if(value.length == 0 || input.length < value.length)
	{
		return false;
	}
	
	return (input[0..value.length] == value);
}

// How on earth do I summarise this function?
/// Formats the given format string and inserts the given arguments into it in a human-readable form.
/// The format for the format string is the same as Phobos' format(Because that's what this function uses).
/// 
/// Parameters:
/// 	format 	= The format for the function's output.
/// 	args	= The arguments to insert into $(B format).
/// 
/// 
/// Throws $(B System.ArgumentNullException) if $(B format) is null.
/// Throws $(B System.FormatException) if either an invalid format string, or invalid parameters(Such as there not being enough parameters) are passed.
/// 
/// Returns:
/// 	A string created from inserting $(B args) properly into $(B format). 
@safe
T Format(T, P...)(in T format, in P args)
if(isSomeString!T)
{
	enforce(format !is null, new ArgumentNullException("format"));

	string Result;
	try
	{
		Result = std.format.format(format, args);
	}
	catch(Exception ex) // Catch any format exception, and throw a DNet version of it(This might also avoid that amazingly large stack trace that format gives when it throws)
	{
		throw new FormatException(ex.msg);
	}

	// Convert the result into the correct version
	static if(is(T == string))
	{
		return Result;
	}
	else static if(is(T == wstring))
	{
		return toUTF16(Result);
	}
	else
	{
		return toUTF32(Result);
	}
}

/// Inserts the given value into the given input at a specified zero-based index.
/// The value is appended to the input if the specified index is the same as the input's length.
/// 
/// Parameters:
/// 	input = The string that will get a value inserted into.
/// 	index = The index within $(B input) that the value is inserted into.
/// 	value = The value that is inserted into $(B input).
/// 
/// 
/// Throws $(B System.ArgumentNullException) if either string argument is null.
/// Throws $(B System.ArgumentOutOfRangeException) if $(B index) is larger than the length of $(B input).
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("I dlang".Insert(2, "love ") == "I love dlang");
/// 	}
/// ---
/// 
/// Returns:
/// 	$(B input) with $(B value) inserted at $(B index).
@safe
T Insert(T)(in T input, in size_t index, in T value) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(value !is null, new ArgumentNullException("value"));
	enforce(index <= input.length, new ArgumentOutOfRangeException("index"));

	if(index == input.length)
	{
		return (input ~ value);
	}

	return (input[0..index] ~ value ~ input[index..$]);
}

/// Joins together each element in a given array using a specified seperator between each element.
/// The seperator can be null.
/// 
/// Parameters:
/// 	input = The input to join together.
/// 	seperator = The string to seperate each element in $(B input) with.
/// 
/// 
/// Throws $(B System.ArgumentNullException) if $(B input) is null.
/// 
/// Exanmple:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		string[] Text = ["I", "love", "dlang"];		
/// 
/// 		assert(Text.Join(" ") == "I love dlang");
/// 		assert(Text.Join(null) == "Ilovedlang");
/// 	}
/// ---
/// 
/// Returns:
/// 	The elements in $(B input) concatenated together with $(B seperator) between each element.
@safe
T Join(T)(in T[] input, scope T seperator) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));

	// Make sure seperator is never actually null because I think that could cause issues.
	if(seperator == null)
	{
		seperator = "";
	}

	T Output = "";
	foreach(i, inp; input)
	{
		// If it's the last element, don't add on the seperator to the end.
		if(i == (input.length - 1))
		{
			Output ~= inp;
		}
		else
		{
			Output ~= (inp ~ seperator);
		}
	}

	return Output;
}

// Notice the extreme lack of comments for private stuff.
@safe
private pure 
{
	T Repeat(T)(in T input, in size_t times)
	if(isSomeString!T)
	{
		T Output = "";

		for(size_t i = 0; i < times; i++)
		{
			Output ~= input;
		}

		return Output;
	}

	T Pad(T)(in T input, in size_t desiredSize, in bool left, T padding = " ")
	if(isSomeString!T)
	{
		enforce(input !is null, new ArgumentNullException("input"));

		// Return the input if it's already bigger/as big as the caller wanted.
		if(input.length >= desiredSize)
		{
			return input;
		}

		// Make sure it's never null, and there's at least 1 character, otherwise we'd just have an endless loop.
		if(padding is null)
		{
			padding = " ";
		}

		// Stores the padding, ready to be thinged to the input.
		T Repeated;
		if(padding.length > 1)
		{
			// Below is a (probably over-complicated) way to make sure the padding will keep the output to the desired length. (We don't bother with this if there's only 1 character to the padding)
			// I write these things down so I can try to make sure it all works, and I leave it just in case I have to edit it in anyway.
			/*
			 * input 	= "Tabe"(4)
			 * size  	= 7
			 * padding  = " \t"(2)
			 * 
			 * ToRepeat = ((7 - 4) / 2) + 1
			 * 			= (3 / 2) + 1
			 * 			= 1 + 1
			 * 			= 2
			 * 
			 * Repeated = " \t \t"(4)
			 * 
			 * N 	= (7 - 4)
			 * 		= 3
			 * 
			 * Repeated = [0..3]
			 * 			= " \t "
			 * */
			size_t ToRepeat = ((desiredSize - input.length) / padding.length) + 1;
			Repeated = padding.Repeat(ToRepeat);
			Repeated = Repeated[0..(desiredSize - input.length)];
		}
		else
		{
			Repeated = padding.Repeat(desiredSize - input.length);
		}

		if(left)
		{
			return (Repeated ~ input);
		}
		else
		{
			return (input ~ Repeated);
		}
	}
}

/// Shifts the given input to the right, laeding it with a given padding until the input has reached a specified size.
/// The padding may be incomplete if it contains more than 1 character. No action is taken if the string is larger than $(B desiredSize).
/// 
/// Parameters:
/// 	input = The string to pad.
/// 	desiredSize = The desired size of the string.
/// 	padding = The string to use as the padding. (May be null)
/// 
///
///	Throws $(B System.ArgumentNullException) if $(B input) is null.
///
/// Example:
/// ---
/// 	import System.String;
/// 	
/// 	void Foo()
/// 	{
/// 		string Text = "I love dlang";
/// 		size_t Length = Text.length;
/// 
/// 		assert(Text.PadLeft(Length + 2, "@") == "@@I love dlang");
/// 		assert(Text.PadLeft(0, "soup") == Text);
/// 		assert(Text.PadLeft(Length + 5, "Hi") == "HiHiHI love dlang");
/// 	}
/// ---
///
/// Returns:
/// 	$(B input) that has at least $(B desiredSize) amount of characters, where any extra characters were filled to the left of $(B input) with $(B padding).
@safe
T PadLeft(T)(in T input, in size_t desiredSize, T padding = " ") pure
if(isSomeString!T)
{
	return input.Pad(desiredSize, true, padding);
}

/// Shifts the given input to the left, trailing it with a given padding until the input has reached a specified size.
/// The padding may be incomplete if it contains more than 1 character. No action is taken if the string is larger than $(B desiredSize).
/// 
/// Parameters:
/// 	input = The string to pad.
/// 	desiredSize = The desired size of the string.
/// 	padding = The string to use as the padding.
/// 
///
///	Throws $(B System.ArgumentNullException) if $(B input) is null.
///
/// Example:
/// ---
/// 	import System.String;
/// 	
/// 	void Foo()
/// 	{
/// 		string Text = "I love dlang";
/// 		size_t Length = Text.length;
/// 
/// 		assert(Text.PadRight(Length + 2, "@") == "I love dlang@@");
/// 		assert(Text.PadRight(0, "soup") == Text);
/// 		assert(Text.PadRight(Length + 5, "Hi") == "I love dlangHiHiH"); // Notice that the padding was slightly cut off.
/// 	}
/// ---
///
/// Returns:
/// 	$(B input) that has at least $(B desiredSize) amount of characters, where any extra characters were filled to the right of $(B input) with $(B padding).
@safe
T PadRight(T)(in T input, in size_t desiredSize, T padding = " ") pure
if(isSomeString!T)
{
	return input.Pad(desiredSize, false, padding);
}

/// Replaces all occurances of a given value(Value A) with another given value(Value B).
/// Value B may be null.
/// 
/// Parameters:
/// 	input = The string to perform the replacement in.
/// 	oldValue = The value to replace in $(B input).
/// 	newValue = The value to put in $(B oldValue)'s place.
/// 
/// 
/// Throws $(B System.ArgumentNullException) if $(B input) or $(B oldValue) are null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("I hate dlang".Replace("hate", "love") == "I love dlang");
/// 		assert("Wa-ta-shi wa ba-ka de-su".Replace("-", null) == "Watashi wa baka desu");
/// 	}
/// ---
/// 
/// Returns:
/// 	$(B input) where all occurances of $(B oldValue) have been swapped for $(B newValue).
@safe
T Replace(T)(in T input, in T oldValue, T newValue) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	enforce(oldValue !is null, new ArgumentNullException("oldValue"));

	// Make sure it's never null, because things can go wrong.
	if(newValue is null)
	{
		newValue = "";
	}

	T Output = "";

	ptrdiff_t OldIndex = 0;
	ptrdiff_t Index = input.IndexOf(oldValue);

	// Keep looping while we find an index.
	while(Index != -1)
	{
//		debug
//		{
//			import std.stdio;
//			writefln("Old: %d | New: %d | Output: '%s' | V1: '%s' | V2: '%s'", OldIndex, Index, Output, oldValue, newValue);
//		}

		// Insert the data we skipped past(So we're not cutting out large blocks of text), and then add in the new value
		Output ~= input[OldIndex..Index];
		Output ~= newValue;

		// Skip over the value we're replacing
		OldIndex = (Index + oldValue.length);

		if(OldIndex < input.length)
		{
			// Get the index of the next occurance(Making sure we don't add any number to -1, so the loop stops)
			// Slicing the part of the array we've already been through out, before sending it to IndexOf is hopefully faster than just sending the new string through each time?
			auto Temp = input[OldIndex..$].IndexOf(oldValue);
			Index = ((Temp == -1) ? -1 : (Temp + OldIndex));
		}
		else
		{
			break;
		}
	}
	if(OldIndex < input.length) // The loop above can miss out on the end of the input(Losing data that should be there), so we have this just in case.
	{
		Output ~= input[OldIndex..$];
	}

	return Output;
}

/// A struct containing information on how the String.Split function should perform
struct SplitOptions
{
	/// If true then the Split function will remove any empty strings("")
	bool	RemoveBlanks;

	/// A limit on how many Splits the Split function can do before forcing it to return what it's already got.
	size_t	MaxSplits;
}

/// Splits up a given input using a specified delimiter, optionally with a fixed amount of splits and the removal of empty spaces.
/// The given delimiter can be null.
/// 
/// Parameters:
/// 	input = The string to split
/// 	delimiter = The string to split with(Anything before and after this string are added to the output, but not the string itself)
/// 	options = How the function should perform.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		string Path = "This/Is//Path/To/Cat images";
/// 		
/// 		assert(Path.Split("/") == ["This", "Is", "", "Path", "To", "Cat images"]);
/// 		assert(Path.Split("/", SplitOptions(true, 4) == ["This", "Is", "Path", "To/Cat images"]);
/// 	}
/// ---
/// 
/// Throws $(B System.ArgumentNullException) if $(B input) is null.
/// 
/// Returns:
/// 	An array containing $(B input) where it has been split into two with every occurance of $(B delimiter).
@safe
T[] Split(T)(in T input, in T delimiter, in SplitOptions options = SplitOptions(false, size_t.max)) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));

	// If there's no real delimiter, just return the input.
	if(delimiter is null || delimiter.length == 0)
	{
		return [input];
	}

	T[] Output;

	ptrdiff_t 	OldIndex 	= 0;
	ptrdiff_t 	Index 		= input.IndexOf(delimiter);
	size_t 		Splits 		= 0;

	while(Index != -1 && Splits < options.MaxSplits)
	{
//		import std.stdio;
//		writefln("In: %d | Old: %d | Splits: %d", Index, OldIndex, Splits);

		// Get the data that is before the delimiter.
		auto Temp = input[OldIndex..Index];

		// Skip past the delimiter so we can continue.
		Splits 		+= 1;
		OldIndex 	= (Index + delimiter.length);

		// Calculate the next index.
		auto TempIn	= input[OldIndex..$].IndexOf(delimiter);
		Index 		= (OldIndex >= input.length || TempIn == -1) ? -1 : (TempIn + OldIndex);

		// Ignore any blanks if we've been told to
		if(Temp == "" && options.RemoveBlanks)
		{
			continue;
		}

		Output ~= Temp;
	}
	if(OldIndex < input.length) // We have this here because the loop above may miss out the end of the input
	{
		Output ~= input[OldIndex..$];
	}

	return Output;
}

/// Replaces any textual versions of escape characters into their character form within the string.
/// Any unknown characters are ignored.
/// E.G "Some\\nString" -> "Some\nString"
/// 
/// Parameters:
/// 	input = The string to format.
/// 
/// 
/// Throws $(B System.ArgumentNullException) if $(B input) is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("void\\tGenerateCats(\n)\\n".FormatEscapeCharacters() == "void\tGenerateCats(\n)\n");
/// 	}
/// ---
/// 
/// Returns:
/// 	The $(B input) parameter;
@safe
T FormatEscapeCharacters(T)(in T input) pure
if(isSomeString!T)
{
	enforce(input !is null, new ArgumentNullException("input"));
	T ToReturn = "";

	auto 		Index = input.IndexOf("\\");
	ptrdiff_t 	OldIndex = 0;

	while(Index != -1 && OldIndex < (input.length - 1))
	{
		auto Temp = (Index + OldIndex);
		auto Char = input[Temp + 1];
//		debug
//		{
//			import std.stdio;
//			writeln(Char, " ", OldIndex, " ", Index);
//		}

		if(Index > 0)
		{
			ToReturn ~= input[OldIndex..Index];
		}

		switch(Char)
		{
			case 'n':
				ToReturn ~= "\n";
				break;

			case '0':
				ToReturn ~= "\0";
				break;

			case 't':
				ToReturn ~= "\t";
				break;

			case 'r':
				ToReturn ~= "\r";
				break;

			case '\\':
				ToReturn ~= "\\";
				break;

			case 'b':
				ToReturn ~= "\b";
				break;

			default:
				ToReturn ~= input[Temp..Temp + 2];
				break;
		}

		Index += 2;
		OldIndex += Index;

		if(OldIndex >= input.length)
		{
			break;
		}

		Index = input[OldIndex..$].IndexOf("\\");
	}
	if(OldIndex != input.length) ToReturn ~= input[OldIndex..$];

	return ToReturn;
}

/// Returns how many times a certain string appears in a given input.
/// 
/// Parameters:
/// 	input = The string to search through.
/// 	toCount = The string to count the occurences of.
/// 
/// 
/// Throws $(B System.ArgumentNullException) if either parameter is null.
/// 
/// Example:
/// ---
/// 	import System.String;
/// 
/// 	void Foo()
/// 	{
/// 		assert("0 1 2 3 4 5 6 7".CountOf(" ") == 7);
/// 		assert("abc bca cab".CountOf("a") == 3);
/// 	}
/// ---
/// 
/// Returns:
/// 	How many times $(B toCount) shows up in $(B input).
@safe
size_t CountOf(T)(T input, T toCount) pure
if(isSomeString!T)
{
	enforce(toCount !is null, new ArgumentNullException("toCount"));
	enforce(input !is null, new ArgumentNullException("input"));

	if(toCount == "")
	{
		return 0;
	}

	size_t 		Count;
	ptrdiff_t	Index = input.IndexOf(toCount);

	while(Index != -1)
	{
		Count += 1;
		Index += toCount.length;

		if(Index >= input.length)
		{
			break;
		}

		auto Temp = input[Index..$].IndexOf(toCount);
		Index = (Temp == -1) ? -1 : (Index + Temp);
	}

	return Count;
}

unittest
{
	string SString 	= "Dlang";
	wstring WString	= "I love programming in D"w;
	dstring DString = "Unittests are amazing"d;

	assert(SString.Contains("D"));
	assert(WString.Contains(" in D"w));
	assert(DString.Contains("tests are amazing"d));
	assert(!SString.Contains(" Dlang"));
	assert(!WString.Contains(""w));
	assert(!DString.Contains("exams are amazing"d));

	assert(SString.EndsWith("ang"));
	assert(WString.EndsWith(" in D"));
	assert(DString.EndsWith("tests are amazing"));
	assert(!SString.EndsWith(" Dlang"));
	assert(!WString.EndsWith(""));
	assert(!DString.EndsWith("exams are amazing"));

	assert(SString.IndexOf("Dl") == 0);
	assert(WString.IndexOf("love") == 2);
	assert(DString.IndexOf("are") == ("Unittests".length + 1)); // Too lazy to figure out the number
	assert(SString.IndexOf(" Dlang") == -1);
	assert(WString.IndexOf("stupid") == -1);
	assert(DString.IndexOf("") == -1);

	import std.algorithm, std.string;
	auto Temp = WString[1..$].dup;
	reverse(Temp);

	auto Debug = WString.IndexOfAny([Temp, "ming in D", Temp, "love", Temp]);

	assert(SString.IndexOfAny(["lang", null, "", "D"]) == 0);
	assert(Debug == 2, format("%d", Debug));
	assert(DString.IndexOfAny(["amazing", "are"]) == ("Unittests".length + 1));
	assert(SString.IndexOfAny([null, ""]) == -1);
	assert(DString.IndexOfAny(["SUper amazing nothing"]) == -1);

	assert(SString.StartsWith("Dla"));
	assert(WString.StartsWith("I love"));
	assert(DString.StartsWith("Unit"));
	assert(!SString.StartsWith(""));
	assert(!DString.StartsWith("C++ is a programming language."));

	assert(Format("I love %s and %s, and the number %d.", "Programming", "Dlang", 13) == "I love Programming and Dlang, and the number 13.");
	assert(Format("My name is %s%s"w, "Sealab"w, "Jaster") == "My name is SealabJaster"w);
	assert(Format("I am %s coding in the %s language, and I like %s."d, "SealabJaster"d, "D"w, "Dogs") == "I am SealabJaster coding in the D language, and I like Dogs."d);

	// 100% coverage is needed!... For science
	try
	{
		Format("Love me %s.");
		assert(false);
	}
	catch(FormatException ex)
	{
	}

	assert(SString.Insert(SString.length, SString) == (SString ~ SString));
	assert(WString.Insert(2, "don't ") == "I don't love programming in D");

	assert([SString, SString].Join("->") == "Dlang->Dlang");
	assert([DString, DString].Join(null) == (DString ~ DString));

	assert(SString.PadLeft(6, null) == " Dlang", Format("'%s'", SString.PadLeft(6)));
	assert("Tabe"d.PadLeft(7, " \t") == " \t Tabe"d,
		Format("%s", cast(ubyte[])"Tabe".PadLeft(7, " \t"))
		);
	assert(WString.PadLeft(WString.length) == WString);

	assert(SString.PadRight(6) == "Dlang ");
	assert("Tabe"d.PadRight(7, " \t") == "Tabe \t "d);
	assert(WString.PadRight(WString.length) == WString);

	assert(SString.Replace("lang", "bang") == "Dbang");
	assert(WString.Replace("i", "u") == "I love programmung un D"w);
	assert(DString.Replace("a", null) == "Unittests re mzing"d);

	assert(SString.Split("") == [SString]);
	assert(WString.Split("m") == ["I love progra"w, "", "ing in D"w], Format("%s", WString.Split("m")));
	assert("\t\t\t\t"d.Split("\t", SplitOptions(true, 2)) == ["\t\t"d]);

	assert("I\\tHate\nDabiel(I don't)".FormatEscapeCharacters() == "I\tHate\nDabiel(I don't)", "I\\tHate\nDabiel(I don't)".FormatEscapeCharacters());
	assert("\\n\\0\\t\\r\\\\\\b\\g".FormatEscapeCharacters() == "\n\0\t\r\\\b\\g");

	assert(WString.CountOf("m") == 2);
	assert(SString.CountOf("Dlang") == 1);
	assert("1 2 3 4 5 6 7 8".CountOf(" ") == 7);
}