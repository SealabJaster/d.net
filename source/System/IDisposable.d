﻿module System.IDisposable;

/// Provides a mechanism to release resources.
interface IDisposable
{
	/// Release, free, and reset any resources as neccissary.
	void Dispose();

	/// Determines whether the $(B Dispose) method has been called.
	@property
	bool IsDisposed();
}

