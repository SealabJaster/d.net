﻿module System.NotSupportedException;

public
{
	import System.InvalidOperationException;
}

/// The exception that is thrown when an invoked method is not supported, or when there is an attempt to read, seek, or write to a stream that does not support the invoked functionality.
class NotSupportedException : InvalidOperationException
{
	@safe
	pure nothrow
	{
		/// Constructs a NotSupportedException with a default error message.
		this()
		{
			super("An unsupported operation was attempted.");
		}

		/// Constructs a NotSupportedException with a given error message.
		/// 
		/// Parameters:
		/// 	message = The error message to show.
		this(string message)
		{
			super(message);
		}
	}
}

