﻿module System;

public
{
	import System.SystemException, System.ArgumentException, System.IDisposable,
		System.ArgumentNullException, System.BitConverter, System.InvalidOperationException,
		System.NotSupportedException, System.ObjectDisposedException, System.ICloneable,
		System.String, System.FormatException;
}