﻿module System.ArgumentNullException;

public
{
	import System.ArgumentException;
}

/// The exception that is thrown when a null reference is passed to a method that does not accept it as a valid argument. 
class ArgumentNullException : ArgumentException
{
	@safe
	pure nothrow
	{
		/// Constructs an ArgumentNullException with a default error message.
		this()
		{
			super("The parameter is null.");
		}

		/// Constructs an ArgumentNullException with the given name of the parameter causing the exception.
		/// 
		/// Parameters:
		/// 	parameter = The parameter that caused the exception.
		this(string parameter)
		{
			super("The parameter is null.", parameter);
		}
	}
}

