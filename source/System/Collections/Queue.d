﻿module System.Collections.Queue;

private
{
	import std.exception, std.functional;
}

public
{
	import System.ICloneable, System.InvalidOperationException;
}

/// Implementation of a Queue data structure(First in first out)
class Queue(T) : ICloneable
{
	@safe
	private pure
	{
		T[]		_Buffer;
		size_t	_Index;
		size_t	_Step;

		this(T[] b, size_t i, size_t s)
		{
			this._Buffer = b;
			this._Index = i;
			this._Step = s;
		}
	}

	@safe
	public pure
	{
		/// Constructs a new Queue with the given initial size for it's buffer.
		/// 
		/// Parameters:
		/// 	initialSize = The inital size of the Queues's buffer.
		/// 	allocataionStep = How many more elements the can hold when it's buffer has to increase in size(Set to 0 to prevent any allocation).
		this(size_t initialSize = 0, size_t allocationStep = 1)
		{
			this._Buffer.length = initialSize;
			this._Step = allocationStep;
		}

		/// Creates a shallow copy of the current Queue.
		Queue!T Clone()
		{
			return new Queue(this._Buffer, this._Index, this._Step);
		}
		
		/// Clears the Queue, it's capacity isn't affected.
		void Clear()
		{
			this._Index = 0;
		}

		/// Adds an element to the end of the queue
		/// 
		/// Parameters:
		/// 	element = The element to add.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if the queue's buffer is full, and it's allocation step is 0.
		void Enqueue(T element)
		{
			if(this._Step == 0) enforce(this._Index != this._Buffer.length, new InvalidOperationException("The end of the queue has been reached, and the current instance has been disallowed from allocating more space."));

			if(this._Index == this._Buffer.length)
			{
				this._Buffer.length += this._Step;
			}

			this._Buffer[this._Index++] = element;
		}

		/// Removes and returns the element at the start of the queue.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if the queue is empty.
		/// 
		/// Returns:
		/// 	The element at the start of the queue.
		T Dequeue()
		{
			enforce(!this.Empty, new InvalidOperationException("The stack is empty"));

			T ToReturn = this._Buffer[0];

			if(this._Index > 1)
			{
				this._Buffer[0..this._Index - 1] = this._Buffer[1..this._Index];
			}

			this._Index--;
			return ToReturn;
		}

		/// Returns the element at the start of the queue without removing it.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if the queue is empty.
		/// 
		/// Returns:
		/// 	The element at the start of the queue.
		T Peek()
		{
			enforce(!this.Empty, new InvalidOperationException("The stack is empty"));

			return this._Buffer[0];
		}
		
		/// Determines whether a given element is in the Queue, using the given condition string to determine whether it exists.
		/// 
		/// Parameters:
		/// 	[condition] = The condition string to apply to $(B element) and the elements in the stack to determine if $(B element) exists.
		/// 	element = The element to check for.
		/// 
		/// Returns:
		/// 	Whether $(B element) is in the array.
		bool Contains(string condition = "a == b")(in T element)
		{
			if(this._Index == 0) return false;
			
			alias Condition = binaryFun!(condition);
			
			foreach(ele; this._Buffer[0..this._Index])
			{
				if(Condition(element, ele))
				{
					return true;
				}
			}
			
			return false;
		}
		
		/// Get whether the Queue is empty.
		@property
		bool Empty()
		{
			return (this._Index == 0);
		}
		
		/// Get how many elements in total can be stored before an allocation must take place for the Queue's buffer.
		@property
		size_t Capacity()
		{
			return this._Buffer.length;
		}
		
		/// Get how many elements are currently on the Queue.
		@property
		size_t Count()
		{
			return this._Index;
		}
		
		/// Get the slice of the Queue's buffer that currently holds elements.
		/// Null is returned if the Queue is empty.
		/// The bottom of the stack is the far left, the top is the far right.
		@property
		T[] Array()
		{
			return (this.Empty ? null : this._Buffer[0..this._Index]);
		}
		
		/// Get the Queue's buffer.
		@property
		T[] Buffer()
		{
			return this._Buffer;
		}
	}
}
unittest
{
	struct A
	{
		uint	_Number;
		string	_Sex;
	}
	auto Queu = new Queue!A();
	auto A1 = A(253, "Dan is Soupy.");
	auto A2 = A(253, "Dan is not Soupy.");
	
	Queu.Enqueue(A1);
	Queu.Enqueue(A2);
	
	assert(Queu.Dequeue() == A1);
	assert(Queu.Peek() == A2);
	assert(Queu.Contains(A2) && !Queu.Contains(A1));
	assert(Queu.Array[0] == A2);
	assert(Queu.Buffer[0] == A2);
	assert(Queu.Count == 1);
	assert(!Queu.Empty);

	Queu.Clear();
	assert(Queu.Count == 0);
	assert(Queu.Capacity == 2);

	auto Q2 = Queu.Clone();
	Q2.Enqueue(A1);
	assert(Q2.Array[0] == Queu.Buffer[0]); // Any clone that shares a buffer doesn't(Can't really) update the index of other clones, which could certainly be bothersome... Should I dup the buffer for every clone?
}