﻿module System.Collections.BitArray;

private
{
	import std.exception;
}

public
{
	import System.ICloneable;
}

/// Manages an array of bytes and provides an interface to manipulate the bits of the bytes.
class BitArray : ICloneable
{
	private
	{
		ubyte[] _Data;

		@safe
		void Operation(string Op)(scope BitArray array) pure // Clearly my understand of "pure" is wrong... I thought it wasn't allowed to access anything outside the function?
		{
			// Figure out which BitArray has the smaller size
			size_t Length = (array.Bytes.length >= this._Data.length) ? this._Data.length : array.Bytes.length;
			
			for(size_t i = 0; i < Length; i++)
			{
				static if(Op == "AND")
				{
					this._Data[i] &= array.Bytes[i];
				}
				static if(Op == "OR")
				{
					this._Data[i] |= array.Bytes[i];
				}
				static if(Op == "XOR")
				{
					this._Data[i] ^= array.Bytes[i];
				}
			}
		}
	}

	@safe
	public pure
	{
		/// Constructs a BitArray with a specified number of bytes.
		/// 
		/// Parameters:
		/// 	numberOfBytes = The number of bytes that the array should start off with.
		this(size_t numberOfBytes)
		{
			this.Length = numberOfBytes;
		}

		/// Sets the bit at the specified to index to 1 or 0 depending on the value of $(B value).
		/// 
		/// Parameters:
		/// 	bitIndex = The index of the bit to set
		/// 	value = The value to set the bit to. $(B true) sets the bit to $(B 1). $(B false) sets the bit to $(B 0)
		void Set(in size_t bitIndex, in bool value)
		{
			// Figure out which byte holds the bit we want and where the bit will be in the byte.
			size_t Byte = (bitIndex / 8);
			size_t BitIndex = (bitIndex % 8);

			if(value == true)
			{
				this._Data[Byte] |= (0x01 << BitIndex);
			}
			else
			{
				this._Data[Byte] &= ~(0x01 << BitIndex);
			}
		}

		/// Sets all of the bits in the BitArray to $(B value)
		/// 
		/// Paramters:
		/// 	value = The value to set all the bits
		void SetAll(in bool value)
		{
			this._Data[] = (value == true) ? 0xFF : 0x00;
		}

		/// Gets the value of the bit at $(B bitIndex), represented as a boolean
		/// 
		/// Parameters:
		/// 	bitIndex = The index of the bit to get the value of.
		/// 
		/// Returns:
		/// 	The value of the bit at $(B bitIndex). $(B true) means the bit is $(B 1). $(B false) means that the bit is $(B 0)
		bool Get(in size_t bitIndex)
		{
			size_t Byte = (bitIndex / 8);
			size_t BitIndex = (bitIndex % 8);

			return cast(bool)(this._Data[Byte] & (0x01 << BitIndex));
		}

		/// Makes a clone of the BitArray
		/// 
		/// Returns:
		/// 	A clone of the BitArray
		BitArray Clone()
		{
			auto ToReturn = new BitArray(this._Data.length * 8);
			ToReturn._Data = this._Data.dup;
			return ToReturn;
		}

		/// Performs a bitwise AND operation with the current BitArray's bytes and $(B toAndWith)'s bytes, the output of each operation is stored in the current BitArray.
		/// .
		/// Say the current BitArray had 4 bytes, and $(B toAndWith) had 8, only the first 4 bytes of the current BitArray would be ANDed by the first 4 bytes of $(B toAndWith).
		/// 
		/// Parameters:
		/// 	toAndWith = The BitArray to perform the AND operation on.
		alias Operation!("AND") And;

		/// Performs a bitwise OR operation with the current BitArray's bytes and $(B toOrWith)'s bytes, the output of each operation is stored in the current BitArray.
		/// .
		/// Say the current BitArray had 4 bytes, and $(B toOrWith) had 8, only the first 4 bytes of the current BitArray would be ORed by the first 4 bytes of $(B toOrWith).
		/// 
		/// Parameters:
		/// 	toOrWith = The BitArray to perform the OR operation on.
		alias Operation!("OR")  Or;
		alias Operation!("XOR") Xor;

		/// Returns the bytes that the BitArray is using
		@property
		ubyte[] Bytes()
		{
			return this._Data;
		}

		/// Sets the length of the BitArray in bytes.
		@property
		void Length(in size_t numberOfBytes)
		{			
			this._Data.length = numberOfBytes;
		}

		/// Gets the length of the BitArray in bytes.
		@property
		size_t Length()
		{
			return this._Data.length;
		}

		/// Sets the bit at $(B index) to $(B value)
		void opIndexAssign(in bool value, in size_t index)
		{
			this.Set(index, value);
		}

		/// Gets the value of the bit at $(B index)
		bool opIndex(in size_t index)
		{
			return this.Get(index);
		}

		/// Sets the value of the bits $(B index1..index2) to $(B value)
		void opSliceAssign(in bool value, in size_t index1, in size_t index2)
		{
			foreach(bit; index1..index2)
			{
				this[bit] = value;
			}
		}

		/// Returns the values of the bits $(B index1..index2)
		bool[] opSlice(in size_t index1, in size_t index2)
		{
			bool[] ToReturn;
			foreach(bit; index1..index2)
			{
				ToReturn ~= this[bit];
			}

			return ToReturn;
		}

		void opOpAssign(string Op)(scope BitArray array)
		{
			static if(Op == "~")
			{
				this._Data ~= array._Data;
			}
			static if(Op == "&")
			{
				this.And(array);
			}
			static if(Op == "|")
			{
				this.Or(array);
			}
			static if(Op == "^")
			{
				this.Xor(array);
			}
		}
	}
}
unittest
{
	import std.string;

	auto Bits = new BitArray(1);

	Bits.Set(0, true); // 0x01
	Bits.Set(1, true); // 0x03
	Bits.Set(2, true); // 0x07
	Bits.Set(1, false); // 0x05

	assert(Bits.Bytes[0] == 0x05, format("0x%X", Bits.Bytes[0]));
	assert(Bits.Get(0) == true);
	assert(Bits.Get(1) == false);
	assert(Bits.Get(2) == true);

	Bits.SetAll(true);
	assert(Bits.Bytes[0] == 0xFF);

	Bits.SetAll(false);
	assert(Bits.Bytes[0] == 0x00);

	Bits[5] = true;
	assert(Bits[5] == true);

	Bits[0..5] = true;
	assert(Bits[2..4] == [true, true]);

	Bits.SetAll(true);
	auto Bits2 = Bits.Clone();
	assert(Bits2.Bytes[0] == 0xFF);

	Bits2.Bytes[0] = 0x1A;
	Bits2.And(Bits); // 0x1A
	assert(Bits2.Bytes[0] == 0x1A);

	Bits2.SetAll(false); // 0x00
	Bits2.Or(Bits); // 0xFF
	assert(Bits2.Bytes[0] == 0xFF);

	Bits2.SetAll(false); // 0x00
	Bits2 ~= Bits; // 0x00FF
	assert(Bits2.Bytes == [0x00, 0xFF]);

	Bits2.Length = 1; // 0x00
	Bits2 |= Bits; // 0xFF
	assert(Bits2.Bytes[0] == 0xFF);

	Bits.SetAll(false); // 0x00
	Bits.Bytes[0] = 0x1A;
	Bits2 &= Bits; // 0x1A
	assert(Bits2.Bytes[0] == 0x1A);

	Bits2.Length = 4;
	Bits2.Bytes[0] = 0xFF;
	Bits2.Bytes[1] = 0xFE;
	Bits2.Bytes[2] = 0xFD;
	Bits2.Bytes[3] = 0xFC;

	Bits2.Length = 1;
	Bits.Length = 1;

	Bits.SetAll(true); // 	 0xFF, 	1111 1111
	Bits2.Bytes[0] = 0x0F;// 0x0F, 	0000 1111
	Bits ^= Bits2; // 		 0xF0, 	1111 0000

	assert(Bits.Bytes[0] == 0xF0, format("0x%X", Bits.Bytes[0]));
	assert(Bits.Length == 1); // Just for that 100% coverage
} 