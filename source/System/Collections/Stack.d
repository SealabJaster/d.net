﻿module System.Collections.Stack;

private
{
	import std.exception, std.functional;
}

public
{
	import System.ICloneable, System.InvalidOperationException;
}

/// Generic implementation of a stack(Last in first out).
class Stack(T) : ICloneable
{
	@safe
	private pure
	{
		T[]		_Array;
		size_t	_Index;
		size_t 	_Step;

		this(T[] a, size_t i, size_t s)
		{
			this._Array = a;
			this._Index = i;
			this._Step = s;
		}
	}

	@safe
	public pure
	{
		/// Constructs a new Stack with the given initial size for it's buffer.
		/// 
		/// Parameters:
		/// 	initialSize = The inital size of the Stack's buffer.
		/// 	allocataionStep = How many more elements the stack can hold when it's buffer has to increase in size(Set to 0 to prevent any allocation).
		this(size_t initialSize = 0, size_t allocationStep = 1)
		{
			this._Array.length = initialSize;
			this._Step = allocationStep;
		}

		/// Creates a shallow copy of the current Stack.
		Stack!T Clone()
		{
			return new Stack(this._Array, this._Index, this._Step);
		}

		/// Clears the stack, it's capacity isn't affected.
		void Clear()
		{
			this._Index = 0;
		}

		/// Determines whether a given element is in the stack, using the given condition string to determine whether it exists.
		/// 
		/// Parameters:
		/// 	[condition] = The condition string to apply to $(B element) and the elements in the stack to determine if $(B element) exists.
		/// 	element = The element to check for.
		/// 
		/// Returns:
		/// 	Whether $(B element) is in the array.
		bool Contains(string condition = "a == b")(in T element)
		{
			if(this._Index == 0) return false;

			alias Condition = binaryFun!(condition);

			foreach(ele; this._Array[0..this._Index])
			{
				if(Condition(element, ele))
				{
					return true;
				}
			}

			return false;
		}

		/// Pushes a given element onto the stack
		/// 
		/// Parameters:
		/// 	element = The element to add.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if the end of the stack's buffer has been reached, and it's allocation step is 0.
		void Push(T element)
		{
			if(this._Step == 0) enforce(this._Index != this._Array.length, new InvalidOperationException("The end of the stack has been reached, and the current instance has been disallowed from allocating more space."));

			if(this._Index == this._Array.length)
			{
				this._Array.length += this._Step;
			}

			this._Array[this._Index++] = element;
		}

		/// Pops the element from the top of the stack.
		/// 
		/// 
		///
		/// Throws $(B System.InvalidOperationException) if the stack is empty.
		/// 
		/// Returns:
		/// 	The element at the top of the stack.
		T Pop()
		{
			enforce(!this.Empty, new InvalidOperationException("The stack is empty."));

			return this._Array[--this._Index];
		}

		/// Returns the element from the top of the stack without poppping it.
		/// 
		///
		///
		/// Throws $(B System.InvalidOperationException) if the stack is empty.
		/// 
		/// Returns:
		/// 	The element at the top of the stack.
		T Peek()
		{
			enforce(!this.Empty, new InvalidOperationException("The stack is empty."));
			
			return this._Array[this._Index - 1];
		}

		/// Get whether the stack is empty.
		@property
		bool Empty()
		{
			return (this._Index == 0);
		}

		/// Get how many elements in total can be stored before an allocation must take place for the Stack's buffer.
		@property
		size_t Capacity()
		{
			return this._Array.length;
		}

		/// Get how many elements are currently on the stack.
		@property
		size_t Count()
		{
			return this._Index;
		}

		/// Get the slice of the Stack's buffer that currently holds elements.
		/// Null is returned if the stack is empty.
		/// Also, if it wasn't too obvious, this slice can't update itself as the Stack's data changes.
		/// The bottom of the stack is the far left, the top is the far right.
		@property
		T[] Array()
		{
			return (this.Empty ? null : this._Array[0..this._Index]);
		}

		/// Get the Stack's buffer.
		@property
		T[] Buffer()
		{
			return this._Array;
		}
	}
}
unittest
{
	struct A
	{
		uint	_Number;
		string	_Sex;
	}
	auto Stac = new Stack!A();
	auto A1 = A(253, "Dan is Soupy.");
	auto A2 = A(253, "Dan is not Soupy.");

	Stac.Push(A1);
	Stac.Push(A2);

	assert(Stac.Pop() == A2);
	assert(Stac.Peek() == A1);
	assert(Stac.Contains(A1) && !Stac.Contains(A2));
	assert(Stac.Array[0] == A1);
	assert(Stac.Buffer[0] == A1);
	assert(Stac.Count == 1);
	assert(!Stac.Empty);
	
	Stac.Clear();
	assert(Stac.Count == 0);
	assert(Stac.Capacity == 2);
	
	auto S2 = Stac.Clone();
	S2.Push(A1);
	assert(S2.Array[0] == Stac.Buffer[0]);
}