﻿module System.InvalidOperationException;

public
{
	import System.SystemException;
}

/// The exception that is thrown when a method call is invalid for the object's current state.
class InvalidOperationException : SystemException
{
	@safe
	pure nothrow
	{
		/// Constructs an InvalidOperationException with a default error message.
		this()
		{
			super("An unknown invalid operation occured.");
		}

		/// Constructs an InvalidOperationException with a given error message
		/// 
		/// Parameters:
		/// 	message = The error message to show.
		this(string message)
		{
			super(message);
		}
	}
}

