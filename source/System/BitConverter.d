﻿module System.BitConverter;

private
{
	import std.exception;
	import std.string : format;
	import std.traits : isSomeChar, isArray, isStaticArray, isDynamicArray, isAssociativeArray, isSomeFunction;
}

public
{
	import System.InvalidOperationException, System.ArgumentNullException;
}

/// A class providing static methods to convert one type of data to another, and swapping the byte order of data.
static class BitConverter
{
	@trusted // At least, from any use I've made (Either in the library or private hobby project) it doesn't seem to cause any issues.
	public static pure
	{
		/++
		 + 	Extension method. Not a part of .Net
		 + .
		 + 	Swaps the order of the bytes that make up the data in a given array.
		 + .
		 + 	For Associative arrays, only it's values are swapped, it's keys are unharmed.
		 + .
		 + 	For structures, it's fields are iterated through and any valid field (Hopefully anything other than functions) are swapped.
		 + 
		 + Parameters:
		 +  data = The array that holdw the data to swap the byte order of.
		 + 
		 + 
		 + Throws $(B System.ArgumentNullException) if $(B data) is null.
		 + 
		 + Example:
		 + ---
		 + 	import System.BitConverter;
		 + 
		 + 	struct A
		 + 	{
		 + 		ushort X;
		 + 	}
		 + 
		 + 	struct B
		 + 	{
		 + 		A[2] Y;
		 + 	}
		 + 
		 + 	void Foo()
		 + 	{
		 + 		auto Bar = B([A(0x01CB) A(0x2EF1)]);
		 + 		BitConvert.SwapOrder([Bar]);
		 + 
		 + 		assert(B.Y[0].X == 0xCB01);
		 + 		assert(B.Y[1].X == 0xF12E);
		 + 	}
		 + ---
		 + 
		 + Returns:
		 + 	The given array($(B data)).
		 + +/
		T[] SwapOrder(T)(T[] data)
		if(!is(T == class))
		{
			enforce(data !is null, new ArgumentNullException("data"));

			static if(is(T == void) || T.sizeof == 1 || isSomeChar!T) return data;
			else
			{
				static if(is(T == struct)) // For structs, recursively go through their fields and reverse them
				{
					foreach(member; __traits(allMembers, T))
					{
						for(auto i = 0; i < data.length; i++)
						{
							// "this" completely murders things, so we check for it first before anything else
							static if(member == "this")
							{
								continue;
							}
							else
							{
								// Apparently I can't do an alias for data[i].%s E_E
								mixin(format("alias TMemb = T.%s;", member));

								// Skip over functions
								static if(isSomeFunction!(typeof(TMemb)))
								{
									continue;
								}
								else static if(isArray!(typeof(TMemb)))
								{
									static if(isAssociativeArray!(typeof(TMemb)))
									{
										mixin(format("BitConverter.SwapOrder(data[i].%s.values);", member));
									}
									else
									{
										mixin(format("BitConverter.SwapOrder(data[i].%s);", member));
									}
								}
								else
								{
									mixin(format("auto _ = [data[i].%s]; ", member));
									BitConverter.SwapOrder(_);
									mixin(format("data[i].%s = _[0];", member));
								}
							}
						}
					}
				}
				else
				{
					// Go over every piece of data, convert it into bytes, and then sort of "reverse" the bytes.
					for(auto i = 0; i < data.length; i++)
					{
						// I can probably make this more efficient...
						ubyte[] Data = cast(ubyte[])(cast(void[])data)[(T.sizeof * i)..((T.sizeof * i) + T.sizeof)];
						for(auto k = 0; k < (T.sizeof / 2); k++)
						{
							auto A = Data[k];
							Data[k] = Data[$ - (k + 1)];
							Data[$ - (k + 1)] = A;
						}
					}
				}

				return data;
			}
		}

		// In .Net there are methods for only primitive types, I feel this is a lot better to use than that.
		/// Converts a given array into an array of a given type, duplicating the given array if wanted.
		/// 
		/// Parameters:
		/// 	duplicate = If true then the given array will be duplicated before the conversion, so any edits to the returned array doesn't affect the original one. If false then the given array won't be duplicated, meaning any changes to the returned array may affect the original one.
		/// 	data = The data to convert.
		/// 
		/// 
		/// Throws $(B System.InvalidOperationException) if $(B data) doesn't contain enough bytes to convert into $(B T). E.g. if $(B T) was ushort, then $(B data) must be a multiple of ushort.sizeof
		/// Throws $(B System.ArgumentNullException) if $(B data) is null.
		/// 
		/// 
		/// Example:
		/// ---
		///	 	import System.BitConverter;
		/// 
		/// 	struct A
		/// 	{
		/// 		ushort X;
		/// 	}
		///
		/// 	struct B
		/// 	{
		/// 		A[2] Y;
		/// 	}
		/// 
		/// 	struct C
		/// 	{
		/// 		ushort X;
		/// 		ushort Y;
		/// 	}
		/// 
		/// 	void Foo()
		/// 	{
		/// 		auto Bar = B([A(0x01CB) A(0x2EF1)]);
		/// 		auto Bytes = BitConverter.Get!ubyte([Bar]);
		/// 		auto Baz = BitConvert.Get!C([Bar])[0];
		/// 
		/// 		// May not work on some systems, because endianess and I can't be bothered to work out a proper example.
		/// 		assert(Bytes == [0x01, 0xCB, 0x2E, 0xF1]);
		/// 		assert(Baz.X == 0x01CB);
		/// 		assert(Bax.Y == 0x2EF1);
		/// 	}
		/// ---
		/// 
		/// Returns:
		/// 	$(B data) converted to a $(B T)[].
		T[] Get(T, T2)(T2[] data, bool duplicate = false)
		if(!is(T2 == class))
		{
			enforce(data !is null, 									new ArgumentNullException("data"));
			enforce(((data.length * T2.sizeof) % T.sizeof) == 0, 	new InvalidOperationException("The given data must have enough bytes to convert into the requested type."));

//			import std.stdio;
//			writefln("data.length: %d | T: %d | T2: %d", data.length, T.sizeof, T2.sizeof); readln();

			if(duplicate) 	return cast(T[])(cast(void[])data.dup);
			else			return cast(T[])(cast(void[])data);
		}
	}
}
version(unittest)
{
	// Apparently nested structs(I think it only affects structs within functions) that contain functions gain 4 bytes in their size... Is this because of closures?
	struct A
	{
		uint 	X;
		uint	B;
		
		// Making sure functions don't murder things
		void Func(){} 
	}
}
unittest
{
	import std.stdio;

	ubyte[] Test = [0xAB, 0xCD, 0xEF, 0x00];
	ushort[] TestUS = BitConverter.Get!ushort(Test, false);

	//writeln(Test);
	BitConverter.SwapOrder(TestUS);
	//writeln(Test);

	assert(Test == [0xCD, 0xAB, 0x00, 0xEF]);

	Test = [0xAB, 0xCD, 0x84, 0x48, 0x00, 0x11, 0x22, 0x33];
	A[] TestA = BitConverter.Get!A(Test);

	BitConverter.SwapOrder(TestA);
	assert(Test == [0x48, 0x84, 0xCD, 0xAB, 0x33, 0x22, 0x11, 0x00]);

	// Test to make sure this throws
	try
	{
		BitConverter.Get!ushort(cast(ubyte[])[0x00, 0x01, 0x02]);
		assert(false);
	}
	catch(InvalidOperationException ex)
	{
	}

	struct B
	{
		A[2]	Data;
	}
	B TestB;

	Test 		= [0xAB, 0xCD, 0x84, 0x48, 0x00, 0x11, 0x22, 0x33, 0x48, 0x84, 0xCD, 0xAB, 0x33, 0x22, 0x11, 0x00];
	auto Debug 	= Test.dup;
	TestB 		= BitConverter.Get!B(Test)[0];

	auto Result = BitConverter.Get!ubyte(BitConverter.SwapOrder!B([TestB]));
	assert(Result == [0x48, 0x84, 0xCD, 0xAB, 0x33, 0x22, 0x11, 0x00, 0xAB, 0xCD, 0x84, 0x48, 0x00, 0x11, 0x22, 0x33], format("Result: %s\n\nDebug: %s", Result, Debug));
}