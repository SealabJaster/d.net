﻿module System.ArgumentException;

/// This exception is thrown when an argument is not valid.
class ArgumentException : Exception
{
	@safe
	pure nothrow
	{
		/// Constructs an $(B ArgumentException) with a default error message.
		this()
		{
			this("An unknown error occured", "Unspecified");
		}

		/// Constructs an $(B ArgumentException) with a given error message.
		/// 
		/// Parameters:
		/// 	message = The error that occured
		this(string message)
		{
			this(message, "Unspecified");
		}

		/// Constructs an $(B ArgumentException) with a given error message, as well as the name of the parameter that caused the error.
		/// 
		/// Parameters:
		/// 	message = The error that occured.
		/// 	parameter = The name of the parmeter that caused the issue.
		this(string message, string parameter)
		{
			super("Parameter: " ~ message ~ 
				"\nMessage: " ~ parameter);
			//super(format("Parameter: '%s'\nMessage: '%s'", message, parameter));
		}
	}
}