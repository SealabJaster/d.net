﻿module System.FormatException;

public
{
	import System.SystemException;
}

/// The exception that is thrown when the format of a parameter is invalid.
class FormatException : SystemException
{
	@safe
	pure nothrow
	{
		/// Constructs a FormatException with a default error message.
		this()
		{
			super("An attempt to use a disposed object was made.");
		}

		/// Constructs a FormatException with a given error message.
		this(string message)
		{
			super(message);
		}
	}
}

