﻿module System.SystemException;

/// Serves as the base class for system exception.
class SystemException : Exception
{
	@safe
	pure nothrow
	{
		/// Constructs a $(B SystemException) with a default message.
		this()
		{
			super("An unknown error has occured");
		}

		/// Constructs a $(B SystemException) with a given message.
		/// 
		/// Parameters:
		/// 	message = The exception message.
		this(string message)
		{
			super(message);
		}
	}
}

