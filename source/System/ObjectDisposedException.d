﻿module System.ObjectDisposedException;

public
{
	import System.InvalidOperationException;
}

/// The exception that is thrown when an operation is performed on a disposed object.
class ObjectDisposedException : InvalidOperationException
{
	@safe
	pure nothrow
	{
		/// Constructs an ObjectDisposedException with a default error message.
		this()
		{
			super("An attempt to use a disposed object was made.");
		}

		this(string message)
		{
			super(message);
		}
	}
}

