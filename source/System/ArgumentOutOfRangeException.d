﻿module System.ArgumentOutOfRangeException;

public
{
	import System.ArgumentException;
}

/// The exception that is thrown when an argument is outside the allowable range of values for a called function.
class ArgumentOutOfRangeException : ArgumentException
{
	@safe
	pure nothrow
	{
		/// Constructs an ArgumentOutOfRangeException with a default error message.
		this()
		{
			super("The parameter is out of range.");
		}
		
		/// Constructs an ArgumentOutOfRangeException with the given name of the parameter causing the exception.
		/// 
		/// Parameters:
		/// 	parameter = The parameter that caused the exception.
		this(string parameter)
		{
			super("The parameter is out of range.", parameter);
		}
	}
}

