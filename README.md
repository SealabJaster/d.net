# README #

### Summary ###
This is a library that is provides methods and classes that are similar to what .Net has to offer.

I quite often have no ideas on what to code, so I look over some stuff that I might find use for in .Net, and I try to recreate them in D. Sometimes with some changes.

This is just something I work on when I feel like coding, so I have no specific-purpose or direction for the project, I just make in D what I like the look of from .Net.

### Getting Started ###
The easiest way is to use [dub](http://code.dlang.org/getting_started) and adding this project as a dependency in your dub.json file.

There is also a wiki that I will eventually make.

### Other ###
The library seems to run and build perfectly fine on Windows 7 using dmd 2.067.0 however, I can not say how it will perform and build on other operating systems.