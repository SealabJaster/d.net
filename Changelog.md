# Change log #

## v0.4.1 ##
### New features ###
- System.String.Split has been added.
- System.String.CountOf has been added.
- System.String.FormatEscapeCharacters has been added.

- System.IO.TextWriter has been slightly modified to allow you to use it like BinaryWriter(As in, you give it a stream of any kind, instead of it just using it's own MemoryStream)
- System.IO.TextWriter.Text has been removed(Due to the change above, breaking change for any code that uses it.)
- System.IO.TextWriter.BaseStream has been added.
- System.IO.TextReader.ReadNumberString has been added.
- System.IO.Path.GetDirectoryName has been added.

- Using a custom tool, a text file will be generated(Whenever I remember to) which details the code coverage percentage for each file.

- Added System.Serialisation.
- Added System.IO.BufferedStream.
- Added System.Data.Json.
- Removed System.Serialisation.
- Removed System.Data.Json

### Fixes/changes ###
- System.String.Replace shouldn't be able to throw and out of bounds exception now <3

- System.BitConverter.SwapOrder should now compile when using it with structs that contain functions.

- System.IO.Path.Combine would incorrectly place "/" or "\\" if a folder in the path ended in ".something". This is now fixed.
- System.IO.Path.Combine no longer misses out "/" or "\\" when handling an absolute path.
- The functions in System.IO.Path no longer have "isSomeChar" in their contracts(Since they were kind of useless).

- System.IO.MemoryStream.Read will no longer throw a range error(hopefully) when you can't read in enough data to fill the buffer given. (Maths error <3)
- System.IO.MemoryStream.Write will no longer throw any kind of range related error(One can pray) when the MemoryStream can't resize, and you try to write past it's buffer.(It should've done this anyway, but my Maths was way off)

- The library should now compile properly for 64-bit.
- A lot of functions in classes/System.String have been marked @safe, pure, and their parameters have been made in, and scoped.
- Some files now have example comments placed into their normal ones.

- The tool used to output "Code Coverage Report.txt" now omits any file ending in "Exception" as I feel I shouldn't really care much about their coverage.

## v0.4.0 ##
#### Major(ish) ####
- Added the Exceptions *System.FormatException* and *System.ArgumentOutOfRangeException*
- Added *System.String* which holds numerous methods to manipulate strings.

#### Fixes/Small changes ####
- Removed some stray TODOs

## v0.3.0 and below ##
I didn't even think of a change log then...